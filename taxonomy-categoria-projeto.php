<?php
  get_header();
  _partials('_start');
  $term = get_queried_object();
?>

  <div class="flex items-start mt-36 mb-24">
    <div class="w-6/12 text-4xl xl:text-6xl 2xl:text-8xl text-black flex flex-col">
      <h2 class="text-gray-fir">Work in</h2>
      <?php if ( $term ): ?>
      <h3 class="text-black">/<?php echo $term->name; ?></h3>
      <a href="<?php echo get_permalink(7); ?>">←</a>
      <?php endif; ?>
    </div>

    <div class="w-6/12 flex items-start justify-between">
      <?php _partials('_get-terms'); ?> 

      <div class="toggle-btns flex items-center">
        <svg onclick="toggleView(event);" class="cursor-pointer layout-large active" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="8" height="8" fill="#DDDDDD"/>
          <rect y="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" width="8" height="8" fill="#DDDDDD"/>
          <rect width="20" height="20" fill="#DDDDDD"/>
        </svg>
        
        <svg onclick="toggleView(event);" class="mx-2.5 cursor-pointer layout-small" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="8" height="8" fill="#DDDDDD"/>
          <rect y="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" y="12" width="8" height="8" fill="#DDDDDD"/>
        </svg>

        <svg onclick="toggleView(event);" class="cursor-pointer layout-list" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="20" height="2" fill="#DDDDDD"/>
          <rect y="6" width="20" height="2" fill="#DDDDDD"/>
          <rect y="12" width="20" height="2" fill="#DDDDDD"/>
          <rect y="18" width="20" height="2" fill="#DDDDDD"/>
        </svg>
      </div>
    </div>
  </div>

  <div class="flex flex-wrap items-between list__view">
    <?php
      if ( have_posts() ) while ( have_posts() ) : the_post();
      $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
    ?>
      <div class="p-1 w-6/12 ease-linear duration-300 view__grid">
        <div class="overflow-hidden">
          <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-110	transition-all duration-300" href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $url; ?>);"></a>
        </div>
      </div>

      <a class="view__list hidden w-full flex border-t border-gray-thi border-solid last:border-b ease-linear duration-300 hover:border-black hover:underline text-xl text-black p-2.5" href="<?php the_permalink(); ?>">
        <div class="w-3/12 font-black"><?php the_title() ?></div>
        <div class="w-3/12">
          <?php
            $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'categoria-projeto' );
            foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
            echo implode(", ", $terms_name);
          ?>
        </div>
        <div class="w-3/12">
          <?php
            $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'area-projeto' );
            foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
            echo implode(", ", $terms_name);
          ?>
        </div>
        <div class="w-3/12 text-right">→ View Case Study</div>
      </a>
    <?php endwhile; ?>  
  </div>

  <?php
  _partials('_lets-work-together');
  _partials('_js-accordion-works');
  _partials('_end');
  get_footer();