<form role="search" method="get" class="flex border-b-2 border-black" action="<?php echo home_url( '/' ); ?>">
  <button class="" type="submit"><i class="fa fa-search"></i></button>
  <input type="search" class="flex-1 bg-inherit outline-0 p-2.5 text-xl" placeholder="O que você procura?" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
</form>