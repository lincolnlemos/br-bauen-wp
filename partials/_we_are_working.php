<?php
  global $item;
  $left = ( is_front_page() ) ? 'w-full md:w-6/12 my-5 md:my-0' : 'w-full md:w-3/12 my-5 md:my-0';
  $center = ( is_front_page() ) ? 'w-full md:w-3/12 my-5 md:my-0' : 'w-full md:w-6/12 my-5 md:my-0';
?>

<div class="flex flex-wrap -mx-2.5 -mb-2.5">
  <div class="<?php echo $left; ?>" data-aos="fade-up">
    <?php if ( is_front_page() ) : ?>
      <div class="text-3xl md:text-4xl lg:text-5xl xl:text-6xl 4k:text-8xl">
        Where here,
        and everywhere.
      </div>
    <?php else : ?>
      <ul class="flex flex-col items-start socials">
        <li class="hover-line"><a class="font-black" target="_blank" href="<?php the_field('facebook', 'options'); ?>">Facebook</a></li>
        <li class="hover-line"><a class="font-black" target="_blank" href="<?php the_field('instagram', 'options'); ?>">Instagram</a></li>
        <li class="hover-line"><a class="font-black" target="_blank" href="<?php the_field('behance', 'options'); ?>">Behance</a></li>
      </ul>
    <?php endif; ?>
  </div>
  <div class="<?php echo $center; ?>" data-aos="fade-up" data-aos-delay="150">
    <?php
      foreach ($item['rpt_map'] as $location) :
      $country = $location['marker']['country'];
    ?>
      <h3 class="text-xl md:text-2xl lg:text-3xl xl:text-4xl 4k:text-5xl"><?php echo $country; ?></h3>
    <?php endforeach ?>
  </div>
  <div class="w-full md:w-3/12 text-xl" data-aos="fade-up" data-aos-delay="300">
    <div class="content"><?php echo $item['description']; ?></div>
    <div class="flex flex-col items-start">
      <?php
        $mail = get_field('email_info', 'option');
        $phone = get_field('phone_info', 'option');
      ?>
      <a class="hover-line" href="tel:<?php echo $phone; ?>">Call: +<span class="phone_with_ddd"><?php echo $phone; ?></span></a>
      <a class="hover-line" href="mailto:<?php echo $mail; ?>">E-mail: <?php echo $mail; ?></a>
      <a class="mt-5 hover-line uppercase" href="<?php echo get_permalink(); ?>">Get in touch →</a>
    </div>
  </div>

  <div class="w-full mt-6 relative" data-aos="fade-up">
    <!-- <div class="fullscreen-control absolute inset-0 bg-black bg-opacity-40 z-10 flex items-center justify-center cursor-pointer">
      <button class="text-gray-fir text-base uppercase">Click Anywhere to navigate</button>
    </div> -->
    <div class="acf-map">
      <?php
        foreach ($item['rpt_map'] as $location) :
          $address        = $location['marker']['address'];
          $city           = $location['marker']['city'];
          $state          = $location['marker']['state'];
          $state_short    = $location['marker']['state_short'];
          $country        = $location['marker']['country'];
          $country_short  = $location['marker']['country_short'];
          $type           = $location['type'];
          $ico            = get_images_url('ico-'.$type.'.svg');
          $thumbMaps      = $location['thumb']['url'];
      ?>
        <div
          class="marker"
          data-lat="<?php echo $location['marker']['lat']; ?>"
          data-lng="<?php echo $location['marker']['lng']; ?>"
          data-ico="<?php echo $ico; ?>"
        >
          <?php if ( $thumbMaps ): ?>
            <img src="<?php echo $thumbMaps; ?>" class="">
          <?php else : ?>
            <h3 class="uppercase font-black"><?php echo esc_html( $country ); ?></h3>
          <?php endif; ?>
        </div>
      <?php endforeach ?>
    </div>
  </div>
</div>