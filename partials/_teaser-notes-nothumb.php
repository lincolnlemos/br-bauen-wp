<article class="text-black text-xl mb-16" data-aos="fade-up">
  <a href="<?php the_permalink(); ?>">
    <h2 class="text-3xl md:text-5xl my-2.5">
      <span class="hover-line md:hover-line-4x"><?php the_title(); ?></span>
    </h2>
    <p class="text-xl">
      <?php
        echo get_the_time('d.m.Y');
        $categories = get_the_category();
        if ( $categories ) : 
          echo ' — ' . esc_html( $categories[0]->name );
        endif;
      ?>
    </p>
  </a>
</article>