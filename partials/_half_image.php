<?php global $item; ?>
<div class="flex flex-wrap" data-aos="fade-up">
  <?php foreach ($item['gallery'] as $image): ?>
    <div class="px-2 w-6/12 ease-linear duration-300 view__grid">
      <div class="overflow-hidden">
        <div class="flex w-full aspect-square bg-no-repeat bg-center bg-cover" style="background-image: url(<?php echo $image['url']; ?>);"></div>
      </div>
    </div>
  <?php endforeach; ?>
</div>