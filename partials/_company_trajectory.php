<?php global $item; ?>
<div class="flex flex-wrap my-36 text-black mb-48">
  <div class="w-full xl:w-1/2 text-xl" data-aos="fade-up">
    <?php if ( !empty($item['founded_in']) ): ?>
      <p class="flex items-start justify-between mb-10">Founded in<span class="counter opacity-0 w-1/2 4k:w-1/2 text-4xl lg:text-6xl 2xl:text-8xl"><?php echo $item['founded_in']; ?></span></p>
    <?php endif; ?>
    <?php if ( !empty($item['countries_served']) ): ?>
      <p class="flex items-start justify-between mb-10">Countries Served<span class="counter opacity-0 w-1/2 4k:w-1/2 text-4xl lg:text-6xl 2xl:text-8xl"><?php echo $item['countries_served']; ?></span></p>
    <?php endif; ?>
    <?php if ( !empty($item['national_international_awards']) ): ?>
      <p class="flex items-start justify-between mb-10">National and International Awards<span class="counter opacity-0 w-1/2 4k:w-1/2 text-4xl lg:text-6xl 2xl:text-8xl"><?php echo $item['national_international_awards']; ?></span></p>
    <?php endif; ?>
  </div>
  <div class="w-full xl:w-1/2" data-aos="fade-up" data-aos-delay="300">
    <h2 class="text-4xl lg:text-6xl 2xl:text-8xl"><?php echo $item['title']; ?></h2>
  </div>
</div>

<script>
  function isCounterElementVisible($element) {
    var topView = $(window).scrollTop();
    var botView = topView + $(window).height();
    var topElement = $element.offset().top;
    var botElement = topElement + $element.height();
    return ((botElement <= botView) && (topElement >= topView));
  }

  $(window).scroll(function() {
    $(".counter").each(function() {
      isOnView = isCounterElementVisible($(this));
      if (isOnView && !$(this).hasClass('opacity-100')) {
        $(this).addClass('opacity-100');
        $(this).prop('Counter', 0).animate({
          Counter: $(this).text()
        }, {
          duration: 3000,
          easing: 'swing',
          step: function(now) {
            $(this).text(Math.ceil(now));
          }
        });
      }
    });
  });
</script>