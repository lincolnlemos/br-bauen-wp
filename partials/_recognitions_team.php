<?php global $item; ?>
<div class="flex justify-end mx-2.5 text-xl my-7">
  <div class="w-1/2 md:w-3/12 font-black" data-aos="fade-up"><?php echo $item['title']; ?></div>
  <div class="w-1/2 md:w-3/12" data-aos="fade-up" data-aos-delay="300">
    <?php
      foreach ($item['rpt_descriptions'] as $description) :
        if ( $description['type'] ) {
          foreach ($description['type'] as $type) {
            // echo '<pre>',print_r($type,1),'</pre>';
            if ( $type['acf_fc_layout'] == 'image' ) {
            ?>
              <div class="flex items-center justify-between mb-5">
                <div class="w-1/2">
                  <img src="<?php echo $type['image']['url']; ?>" />
                </div>
                <div class="w-1/2 flex flex-col border-l border-gray-sec pl-3">
                  <?php
                    if ( $type['link'] ) :
                      echo '<a href="' .$type['link']['url']. '" class="flex flex-col">',
                        $type['text'],
                      '↗</a>';
                    
                    else :
                      echo $type['text'];

                    endif;
                  ?>
                </div>
              </div>
            <?php
            } else {
              echo '<p class="mb-1">'. $type['text'].'</p>';
            }
          }
        }
      endforeach;
    ?>
  </div>
</div>