<?php global $item; ?>
<div class="flex flex-wrap my-16 md:my-36 text-black">
  <div class="w-full md:w-3/12 text-xl" data-aos="fade-up">
    <p class="mb-9"><?php echo $item['description']; ?></p>
    <a class="font-black hover-line" href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a>
  </div>
  <div class="w-full md:w-9/12 mt-10 md:mt-0" data-aos="fade-up" data-aos-delay="300">
    <h2 class="text-4xl xl:text-6xl 2xl:text-8xl"><?php echo $item['title']; ?></h2>
  </div>
</div>