<?php global $item; ?>
<div class="flex flex-wrap py-24">
  <div class="w-1/2 text-8xl" data-aos="fade-up">
    <?php echo $item['title_section']; ?>
  </div>

  <div class="w-1/2" data-aos="fade-up" data-aos-delay="300">
    <?php foreach ($item['rpt_descriptions'] as $section) : ?>
      <div class="flex flex-wrap mb-16">
        <div class="w-1/2"><?php echo $section['title']; ?></div>
        <div class="w-1/2"><?php echo $section['description']; ?></div>
      </div>
    <?php endforeach ?>
  </div>
</div>