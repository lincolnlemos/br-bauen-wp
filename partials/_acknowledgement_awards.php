<?php global $item; ?>
<div class="flex flex-wrap my-72">
  <div class="w-full text-4xl xl:text-6xl 2xl:text-8xl mb-10" data-aos="fade-up" data-aos-offset="200">
    <?php echo $item['title']; ?>
  </div>
  <div class="w-full" data-aos="fade-up" data-aos-offset="200" data-aos-delay="300">
    <?php $i = 0; foreach ($item['rpt_year'] as $year) : $i++; ?>
      <div class="flex border-t border-gray-fou border-solid <?php if ( $i === 1 ) : echo 'border-t-white'; endif; ?> last:border-b text-xl text-black p-5">
        <div class="w-3/12"><?php echo $year['year']; ?></div>
        <div class="w-9/12">
          <div class="element <?php if ( $i === 1 ) : echo 'active'; endif; ?>">
            <div class="question flex flex-wrap cursor-pointer">
              <div class="w-full flex justify-start">
                <div class="w-fit hover-line" data-magicmouse-hover-class="is-anchor size-40">+ Expand</div>
              </div>
              <div class="w-4/12">Recognition</div>
              <div class="w-4/12">Client</div>
              <div class="w-4/12">Type</div>
            </div>
            <div class="answer flex flex-wrap">
              <?php foreach ($year['recognitions'] as $recognition) : ?>
                <div class="flex w-full mb-1.5">
                  <div class="w-4/12"><?php echo $recognition['name_recognition']; ?></div>
                  <div class="w-4/12"><?php echo $recognition['client']; ?></div>
                  <div class="w-4/12"><?php echo $recognition['type']; ?></div>
                </div>
              <?php endforeach; ?>
              <div class="close w-fit mt-10 cursor-pointer hover-line w-fit" data-magicmouse-hover-class="is-anchor size-40">× Close</div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>

<script>
  // pure accordion
  const elements = document.querySelectorAll('.element');
  elements.forEach(element =>{
    let btn = element.querySelector('.question');
    let close = element.querySelector('.close');
    let answer = element.lastElementChild;
    let answers = document.querySelectorAll('.element .answer');
    
    btn.addEventListener('click', ()=>{
      answers.forEach(ans =>{
        if(answer == ans){
          if ( !answer.parentElement.classList.contains('active')) {
            ans.parentElement.classList.add('active');
            ans.parentElement.parentElement.parentElement.classList.add('border-t-white');
            console.log( 'NAO TEM' )
          } else {
            console.log( 'TEM' )
          }
        } 
      });
      answer.classList.toggle('active');
    });

    close.addEventListener('click', ()=>{
      answers.forEach(ans =>{
        if(answer == ans){
          if ( answer.parentElement.classList.contains('active')) {
            ans.parentElement.classList.remove('active');
            ans.parentElement.parentElement.parentElement.classList.remove('border-t-white');
            ans.classList.remove('active');
          }
        } else {
          ans.classList.remove('active');
        }
      });
    });
  });
</script>