<?php
  global $item;
  // echo '<pre>',print_r($item,1),'</pre>';
?>
<div class="flex flex-wrap my-16 md:my-36 text-black">
  <div class="w-full lg:w-3/12 2xl:w-6/12 text-xl" data-aos="fade-up">
    <p class="font-black"><?php echo $item['title']; ?></p>
  </div>
  <div class="w-full lg:w-9/12 2xl:w-6/12 text-xl columns-1 md:columns-2 mt-10 md:mt-0" data-aos="fade-up" data-aos-delay="300">
    <?php echo $item['content']; ?>
  </div>
</div>