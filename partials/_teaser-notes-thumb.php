<?php $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
<article class="w-3/12 p-2.5 text-black text-xl mb-20" data-aos="fade-up">
  <div class="overflow-hidden">
    <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-110	transition-all duration-300" href="<?php echo get_permalink(get_the_ID()); ?>" style="background-image: url(<?php echo $url; ?>);"></a>
  </div>
  <a href="<?php the_permalink(); ?>">
    <h2 class="text-5xl my-2.5 hover:underline underline-offset-8 "><?php the_title(); ?></h2>
    <p class="text-xl">
      <?php
        echo get_the_time('d.m.Y');
        $categories = get_the_category();
        if ( $categories ) : 
          echo ' — ' . esc_html( $categories[0]->name );
        endif;
      ?>
    </p>
  </a>
</article>