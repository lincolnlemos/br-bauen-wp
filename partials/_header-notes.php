<?php
  get_header();
  _partials('_start');

  $posts_page_id = get_option( 'page_for_posts' );
  $post_page_custom_title = get_field('custom_title', $posts_page_id );
  $page_title = $post_page_custom_title ?? get_the_title($posts_page_id);
?>

  <div class="flex flex-wrap items-start my-16 md:my-36">
    <div class="w-full md:w-1/2">
      <?php if (is_home() ) : ?>
        <h2 class="text-4xl xl:text-6xl 2xl:text-8xl"><?php echo $page_title; ?></h2>      
        <?php else: ?>        
        <h2 class="text-4xl xl:text-6xl 2xl:text-8xl">
          <span class="text-gray-fir">Notes on</span> <br />
          <span>/<?php single_term_title(); ?></span>
        </h2>      
      <?php endif; ?>
    </div>
    <div class="w-full md:w-1/4 my-10 md:my-0">
      <ul class="nav-theme-anchor text-xl text-black">
        <?php
          wp_list_categories([
            'title_li' => '',
            'show_option_all' => 'All News',
          ]);
        ?> 
      </ul>
    </div>
    <div class="w-full md:w-1/4">
      <?php get_template_part( 'searchform' ); ?>
    </div>
  </div>