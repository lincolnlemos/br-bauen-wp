<?php global $item; ?>
<div class="projectAccordion flex flex-wrap items-start mx-2.5 text-xl my-11">
  <div class="w-full md:w-1/2 flex flex-wrap blocks" data-aos="fade-up">
    <?php $i = 0; foreach ($item['blocks'] as $block) : $i++; ?>
      <div class="block flex flex-wrap">
        <div class="w-full md:w-1/2 font-black">
          <?php echo $block['title']; ?>
        </div>
        <div class="w-full md:w-1/2 content"><?php echo $block['content']; ?></div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="w-full md:w-1/4 ml-auto flex flex-wrap" data-aos="fade-up" data-aos-delay="150">
    <div class="w-full flex flex-wrap">
      <div class="w-1/2">
        <?php echo __('Industry', 'brbauen') ?>
      </div>
      <div class="w-1/2 flex flex-col items-start">
        <?php
          $terms = get_the_terms( get_the_ID(), 'categoria-projeto' );
          foreach ($terms as $term) :
            echo '<a class="flex" href="#"><span class="hover-line">'.$term->name.'</span></a>';
          endforeach;
        ?>
      </div>
    </div>

    <div class="w-full flex flex-wrap mt-5">
      <div class="w-1/2">
        <?php echo __('Services', 'brbauen') ?>
      </div>
      <div class="w-1/2 flex flex-col items-start">
        <?php echo $item['services']; ?>
      </div>
    </div>
  </div>

  <div class="w-full flex flex-col items-start border-t-2 border-gray-thi border-solid"  data-aos="fade-up" data-aos-delay="300">
    <div class="w-full md:w-3/4 py-5 ml-auto flex justify-start">
      <div class="cursor-pointer more">
        <span class="hover-line">+ More</span>
      </div>
    </div>
  </div>
</div>