<div class="flex justify-start my-36 2xl:my-80" data-aos="fade-up" data-aos-offset="200">
  <a 
    class="text-4xl xl:text-6xl 2xl:text-8xl"
    title="<?php echo __('Get in touch →', 'brbauen') ?>"
    href="<?php echo get_field('lets_work_together_url', 'option');?>"
  >
    <span class="hover-line xl:hover-line-4x 2xl:hover-line-6x">
      <?php echo __('Let’s work together.', 'brbauen') ?><br/>
      <?php echo __('Get in touch →', 'brbauen') ?>
    </span>
  </a>
</div>