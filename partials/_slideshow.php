<div class="flex flex-wrap items-stretch mt-16 md:mt-32 mb-6 md:mb-12 p-2.5">
  <div class="w-full lg:w-6/12 mt-5 lg:mt-0 text-4xl lg:text-5xl xl:text-6xl 2xl:text-8xl	text-black" data-aos="fade-up">
    <?php the_field('slide_title_section'); ?>
  </div>
  <div class="w-full lg:w-6/12 text-xl 4k:w-1/4 flex flex-col justify-between items-start" data-aos="fade-up" data-aos-delay="150">
    <p class="my-4"><?php the_field('slide_description_section'); ?></p>
    <a class="font-black" href="<?php echo get_field('slide_link_section')['url'] ; ?>">
      <span class="hover-line"><?php echo get_field('slide_link_section')['title']; ?></span>
    </a>
  </div>
</div>

<?php 
  if( have_rows( 'rpt_slideshow' ) ): 
    
    function print_slider_media($media, $wrapper_class, $play_video_at_click = '', $index) {
      echo '<div class="'.$wrapper_class.'">';
        $output = '';
        if ($media['type'] == 'video') {                  
           $output .= '<video src="'. $media['url'] .'" autoplay muted loop';
           if ($play_video_at_click) {
            $output .= ' data-magicmouse-video-src="'.$play_video_at_click.'" data-magicmouse-video-wrapper-id="video-wrapper-'.$index.'" ';
           }
          $output .= ' ></video>';
        } else {
          $output .= '<img src="'.$media['url'].'" alt="'.$media['alt'].'"';
           if ($play_video_at_click) {
            $output .= ' data-magicmouse-video-src="'.$play_video_at_click.'" data-magicmouse-video-wrapper-id="video-wrapper-'.$index.'" ';
           }
          $output .= ' />';
        }
        echo $output;
      echo '</div>';
    }

    echo '<div 
      id="js-slideshow-home" 
      class="js-slideshow banner p-2.5 magicmouse-size-60" 
      data-aos="fade-up"
    >';

      while ( have_rows( 'rpt_slideshow' ) ) : the_row();
      $background_media_mobile = get_sub_field('background_media_mobile');
      $media_mobile = get_sub_field('media_mobile');
      $media_mobile_is_a_video = $media_mobile && $media_mobile['type'] == 'video' ? $media_mobile['url'] : false;      
      
      $background_media_desktop = get_sub_field('background_media_desktop');
      $media_desktop = get_sub_field('media_desktop');
      $media_desktop_is_a_video = $media_desktop && $media_desktop['type'] == 'video' ? $media_desktop['url'] : false;
      
      $magicmouse_text = get_sub_field('magicmouse_text') ?? 'Ver Projeto';          

        echo '<div class="js-slideshow__slide">';
          
          echo '<div class="js-slideshow__mediawrapper" data-magicmouse-hover-class="size-120 bg-white" data-magicmouse-text="'.$magicmouse_text.'">';

            print_slider_media($background_media_mobile, 'background-media md:hidden', $media_mobile_is_a_video, get_row_index());
            print_slider_media($background_media_desktop, 'background-media hidden md:block', $media_desktop_is_a_video, get_row_index());
            echo '<video id="video-wrapper-'.get_row_index().'" loop class="hidden" data-magicmouse-text="Stop" />';

          echo '</div>';
          
          echo '<div class="js-slideshow__caption flex my-2.5" data-magicmouse-hover-class="size-20 bg-black">',
                  '<div class="w-1/2 ml-auto text-base xl:text-lg">',
                    '<h4 class="underline underline-offset-8 mb-2.5" data-aos="fade-up" data-aos-delay="200">' . get_sub_field( 'title' ) . '</h4>',
                    '<p data-aos="fade-up" data-aos-delay="300">'. get_sub_field( 'description' ) . '</p>',
                  '</div>',
                '</div>';

        echo '</div>'; // .js-slideshow__slide

      endwhile;

    echo '</div>';
  
  endif;
?> 
