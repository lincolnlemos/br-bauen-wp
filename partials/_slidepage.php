<?php global $item; ?>

<div class="flex flex-wrap slidepage" data-aos="fade-up" data-aos-offset="200" data-aos-delay="300">
  <?php foreach ($item['slide'] as $slide) : ?>
    <div class="flex justify-center"><img src="<?php echo $slide['image']; ?>" class="m-auto" /></div>
  <?php endforeach; ?>
</div>