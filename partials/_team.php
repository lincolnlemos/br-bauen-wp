<?php global $item; ?>
<div class="flex flex-wrap my-36 text-black">
  <div class="w-full md:w-3/12 text-xl" data-aos="fade-up">
    <?php echo $item['description']; ?>
  </div>
  <div class="w-full md:w-9/12 mt-10 md:mt-0" data-aos="fade-up" data-aos-delay="150">
    <h2 class="text-4xl xl:text-6xl 2xl:text-8xl"><?php echo $item['title']; ?></h2>
  </div>
</div>
<div class="flex flex-wrap">
  <?php $delay = 0; foreach ($item['our_team'] as $member): ?>
    <div class="w-full md:w-1/4" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
      <div class="flex flex-col justify-end text-white text-xl aspect-square overflow-hidden bg-no-repeat bg-center bg-cover bg-100% hover:bg-110%	transition-all duration-300 m-1 p-5" style="background-image: url(<?php echo $member['picture']; ?>);">
        <p class="font-black"><?php echo $member['name']; ?></p>
        <p><?php echo $member['profession']; ?></p>
      </div>
    </div>
  <?php $delay+= 50; endforeach; ?>
</div>

<div class="flex flex-wrap my-36">
  <div class="w-1/2 mx-auto flex flex-wrap">
    <div class="w-1/2 flex flex-wrap">
      <?php if ( $item['pastpresent_partners']) : ?>
        <h2 class="mb-12 font-black" data-aos="fade-up">— Past & present partners, collaborators & interns</h2>
        
        <div class="flex flex-col flex-wrap w-full max-h-80">
          <?php $delay = 0; foreach ($item['pastpresent_partners'] as $partner): ?>
            <div class="w-1/2" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
              <?php echo $partner['partner_name']; ?>
            </div>
          <?php $delay+= 50; endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>