<?php global $item; //echo '<pre>',print_r($item,1),'</pre>'; ?>
<div class="flex justify-end mx-2.5 my-16">
  <?php if ( $item['link_to_post']) : ?>
    <div class="w-full md:w-6/12 flex justify-start items-start text-5xl">
      <a class="hover-line xl:hover-line-4x" href="<?php echo $item['link_to_post']['url']; ?>"><?php echo $item['link_to_post']['title']; ?></a>
    </div>
  <?php endif; ?>
  <div class="w-full md:w-6/12">
    <p class="text-xl mb-5" data-aos="fade-up"><?php echo $item['name_role']; ?></p>
    <h4 class="text-4xl md:text-5xl" data-aos="fade-up" data-aos-delay="300"><?php echo $item['testimonial']; ?></h4>
  </div>
</div>