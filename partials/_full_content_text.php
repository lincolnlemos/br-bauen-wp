<?php global $item; ?>
<div class="flex flex-wrap mx-2.5 text-xl my-11">
  <div class="w-full md:w-6/12 text-4xl" data-aos="fade-up"><?php echo $item['title']; ?></div>
  <div class="w-full md:w-6/12 content" data-aos="fade-up" data-aos-delay="300"><?php echo $item['content']; ?></div>
</div>