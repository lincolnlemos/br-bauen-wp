<script>
  function toggleView(e) {
    if(e.target.classList.contains('layout-large') || e.target.parentElement.classList.contains('layout-large')) {
      document.querySelector('.toggle-btns').children[0].classList.add('active');
      document.querySelector('.toggle-btns').children[1].classList.remove('active');
      document.querySelector('.toggle-btns').children[2].classList.remove('active');
      
      document.querySelector('.list__view').classList.add('large');
      document.querySelector('.list__view').classList.remove('small');
      document.querySelector('.list__view').classList.remove('list');

      let elems = document.querySelector('.list__view').children;
      [].forEach.call(elems, function(el) {
        if ( el.classList.contains('view__grid') ) {
          el.classList.remove('w-3/12');
          el.classList.add('w-6/12');

          if ( el.classList.contains('hidden') ) {
            el.classList.remove('hidden');
          }
        } else {
          el.classList.add('hidden');
        }
      });
      
    } else if(e.target.classList.contains('layout-small') || e.target.parentElement.classList.contains('layout-small')) {
      document.querySelector('.toggle-btns').children[0].classList.remove('active');
      document.querySelector('.toggle-btns').children[1].classList.add('active');
      document.querySelector('.toggle-btns').children[2].classList.remove('active');
      
      document.querySelector('.list__view').classList.remove('large');
      document.querySelector('.list__view').classList.add('small');
      document.querySelector('.list__view').classList.remove('list');

      let elems = document.querySelector('.list__view').children;
      [].forEach.call(elems, function(el) {
        if ( el.classList.contains('view__grid') ) {
          el.classList.remove('w-6/12');
          el.classList.add('w-3/12');
          if ( el.classList.contains('hidden') ) {
            el.classList.remove('hidden');
          }
        } else {
          el.classList.add('hidden');
        }
      });
    } else if(e.target.classList.contains('layout-list') || e.target.parentElement.classList.contains('layout-list')) {
      console.log('CLICOU NA LISTA');
      document.querySelector('.toggle-btns').children[0].classList.remove('active');
      document.querySelector('.toggle-btns').children[1].classList.remove('active');
      document.querySelector('.toggle-btns').children[2].classList.add('active');
      
      document.querySelector('.list__view').classList.remove('large');
      document.querySelector('.list__view').classList.remove('small');
      document.querySelector('.list__view').classList.add('list');

      let elems = document.querySelector('.list__view').children;
      [].forEach.call(elems, function(el) {
        if ( el.classList.contains('view__list') ) {
          el.classList.remove('hidden');
        } else {
          el.classList.add('hidden');
        }
      });
    }
  }
</script>