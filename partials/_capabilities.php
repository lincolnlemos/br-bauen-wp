<?php global $item; ?>
<div class="flex text-4xl xl:text-6xl 2xl:text-8xl my-24">
  <?php echo $item['title']; ?>
</div>
<div class="flex flex-wrap text-xl">
  <div class="w-full md:w-3/12 font-black mb-10 md:mb-0" data-aos="fade-up">
    Our Capabilities
  </div>
  <div class="flex flex-wrap items-start w-full md:w-9/12">
    <?php foreach ($item['sector'] as $sector): ?>
      <div class="w-full md:w-2/6" data-aos="fade-up" data-aos-delay="300">
        <p class="mb-8"><?php echo $sector['title']; ?></p>
        <?php foreach ($sector['capabilities'] as $capability): ?>
          <p><?php echo $capability['capability']; ?></p>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>