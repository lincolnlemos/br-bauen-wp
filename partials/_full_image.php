<?php global $item; ?>
<div class="flex w-full aspect-video bg-no-repeat bg-center bg-cover" style="background-image: url(<?php echo $item['image']; ?>);"></div>