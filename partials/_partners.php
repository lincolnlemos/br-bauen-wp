<?php global $item; ?>
<div class="flex flex-wrap py-24" data-aos="fade-up">
  <div class="w-1/2 mx-auto">
    <h4 class="font-black mb-12"><?php echo $item['title_section']; ?></h4>
    <div class="flex flex-wrap">
      <?php foreach ($item['rp_partners'] as $partner) : ?>
        <div class="w-1/2"><?php echo $partner['partner_name']; ?></div>
      <?php endforeach ?>
    </div>
  </div>
</div>