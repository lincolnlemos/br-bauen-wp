<ul class="nav-theme-anchor nav-theme-anchor-gray text-xl text-black">
  <?php
    $currentTerm = get_queried_object();

    $pages = get_pages([
      'meta_key' => '_wp_page_template',
      'meta_value' => 'page-work.php'
    ]);

    $terms = get_terms('categoria-projeto', [
      'post_type' => [ 'projeto' ],
      'fields' => 'all'
    ]);

    foreach($pages as $page){
      if ( $page->ID === get_the_ID() ) {
        $currentClass = 'current-cat';
      } else {
        $currentClass = '';
      }
      echo '<li class="'.$currentClass.'"><a href="'. get_permalink($page->ID).'" class="hover-line">'.$page->post_title.'</a></li>';
    }

    foreach ($terms as $term) :
      if ( $term->term_id == $currentTerm->term_id ) {
        $currentClass = 'current-cat';
      } else {
        $currentClass = '';
      }
      echo '<li class="'.$currentClass.'"><a class="flex hover-line" href="'. get_term_link( $term ) .'">'.$term->name.'<span class="text-xs ml-1 leading-5">'.sprintf("%02d", $term->count).'</span></a></li>';
    endforeach;
  ?>
</ul>