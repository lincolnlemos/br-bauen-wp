<?php
  get_header();
  _partials('_start');
  if ( have_posts() ) while ( have_posts() ) : the_post();
?>

  <article <?php post_class( 'single single__projeto' ); ?>>
    <div class="flex flex-wrap mt-20 lg:mt-40 mb-5" data-aos="fade-up" data-aos-offset="100" data-aos-delay="100">
      <div class="text-4xl md:text-4xl xl:text-6xl 2xl:text-8xl w-full">
        <?php
          if ( get_field('custom_title') ) :
            the_field('custom_title');
          else :
            the_title();
          endif;
        ?>
      </div>
    </div>

    <?php if ( get_field('project_block') ) { ?>
      <div id="contentSingleProject" class="flex flex-col gap-4" data-aos="fade-up" data-aos-offset="100" data-aos-delay="300">
        <?php
          foreach ( get_field('project_block') as $item) {
            global $item;
            _partials('_'.$item['acf_fc_layout']);
          }
        ?>
      </div>
    <?php }; ?>

    <div class="w-full md:w-1/2 ml-auto text-xl" data-aos="fade-up">
      <div class="flex mt-10">
        <span class="w-1/2 font-black">Share</span>
        <?php echo do_shortcode('[social-share]'); ?>
      </div>
    </div>

    <?php _partials('_lets-work-together'); ?>

    <div class="flex flex-wrap my-24">
      <?php
        $loop = new WP_Query([ 'post_type' => 'projeto', 'posts_per_page' => 4, 'post__not_in' => [ get_the_ID() ] ]);
        if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
        $url = wp_get_attachment_url( get_post_thumbnail_id($loop->ID) );
      ?>
        <div class="p-1 w-3/12 ease-linear duration-300 view__grid" data-aos="fade-up">
          <div class="overflow-hidden">
            <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-100	transition-all duration-300" href="<?php the_permalink(); ?>" style="background-image: url(<?php echo $url; ?>);">
              <div class="bg-black w-full py-2 px-2.5 opacity-0 hover:opacity-100 transition-all duration-300">
                <?php
                  $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'categoria-projeto' );
                  foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
                  echo '<p class="text-white">' .get_the_title( get_the_ID() ). '</p>';
                  echo '<p class="text-gray-fir">' .implode(", ", $terms_name). '</p>';
                ?>
              </div>
            </a>
          </div>
        </div>
      <?php endwhile; endif; ?>

      <div class="w-full">
        <a class="text-4xl xl:text-6xl 2xl:text-8xl hover-line xl:hover-line-4x 2xl:hover-line-6x" href="<?php echo get_permalink(7); ?>">More work →</a>
      </div>
    </div>
  </article>

<?php
  endwhile;
  _partials('_end');
  get_footer();