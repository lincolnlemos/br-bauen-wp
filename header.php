<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="<?php echo get_template_directory_uri()	?>/assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <?php wp_head(); ?>
	</head>
	<?php
		$globalBackground = get_field('global_background') ?? 'light-white';
		$globalBackgroundClass = 'global-background-' . $globalBackground		;
		global $wpml_lang;
		$wpml_lang = apply_filters( 'wpml_current_language', null );
	?>
	<body <?php body_class($globalBackgroundClass); ?> style="background-color:#EEEEEE;">
    <div id="page-holder" class="page-loader"> <div id="page-holder--logo"></div></div>
    <div id="page-out" class="page-loader"></div>
    <div id="page-in" class="page-loader"></div>
		<div id="debug-theme-version"><?php echo wp_get_theme()->version;  ?></div>
		<div class="sticky top-0 shadow-lg hidden"></div>
    <header id="header" class="flex flex-wrap justify-between p-2.5 bg-white z-50">
			<div class="w-1/2 md:w-2/12 lg:w-2/12 xl:w-2/12 2xl:w-3/12 4k:w-1/2 flex items-center">
				<?php
					$logotipo = get_field( 'logo_dark', 'option' );
					if ( $logotipo ) :
				?>
					<a class="flex items-center logo-header" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo $logotipo; ?>" />
					</a>
				<?php endif; ?>
			</div>

			<div class="w-1/2 md:w-6/12 lg:w-4/12 xl:w-4/12 2xl:w-3/12 4k:w-3/12 flex justify-end md:justify-between relative">
				<div class="hamburguer">
					<div class="menu__line--one"></div>
					<div class='menu__line--two'></div>
				</div>

				<?php
					wp_nav_menu([
						'menu'            => 'principal',
						'container'       => 'ul',
						'theme_location'  => 'top',
						'menu_class'     	=> 'menu menu__header flex items-center ease-linear duration-300'
					]);
				?>
			</div>

			<div class="w-full md:w-4/12 lg:w-6/12 xl:w-6/12 2xl:w-1/2 4k:w-3/12 hidden md:flex justify-between md:justify-end md:items-center relative text-xl md:text-base xl:text-xl font-black text-black mt-5 md:mt-0">
				<!-- <a class="phone_with_ddd mr-10 md:hidden lg:flex" title="Ligue para nós!" href="tel:<?php $phone = get_field('phone_info', 'option'); echo $phone; ?>">+<?php echo $phone; ?></a> -->
				<?php do_action('wpml_add_language_selector'); ?>
        <?php if ( is_woocommerce() ) :  brbauen_header_cart(); endif; ?>
			</div>

		</header>