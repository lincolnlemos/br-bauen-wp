<?php
	// Define theme constants
	define('BS_PARTIALS_PATH', get_stylesheet_directory() . '/partials/');

	// Defines funcionts of the framework
	require_once ('functions/_setup.php');
	require_once ('functions/_custom.php');
	require_once ('functions/_navwalker.php');
	require_once ('functions/_woo.php');
	require_once ('functions/_api.php');

	// Defines functions about wp-admin
	require_once ('functions/_admin.php');

	// Defines custom functions
	require_once ('functions/_options.php');

	// Register custom post type
	require_once ('cpt/cpt-projects.php');

	// create acf options page
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title'      => 'Theme Options',
			'menu_title'      => '',
			'menu_slug'       => '',
			'capability'      => 'edit_posts',
			'position'        => false,
			'parent_slug'     => '',
			'icon_url'        => false,
			'redirect'        => true,
			'post_id'         => 'options',
			'autoload'        => false,
			'update_button'		=> __('Update', 'acf'),
			'updated_message'	=> __("Options saved", 'acf'),    
		));
	}
	
	// disable gutenberg editor
	add_filter('use_block_editor_for_post', '__return_false', 10);
	add_filter('wpcf7_autop_or_not', '__return_false');

	add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    return $content;
	});


	function wp_list_categories_highlight_all( $output, $args ) {
    if ( array_key_exists( 'show_option_all', $args ) && $args['show_option_all'] ) {
        if ( ! array_key_exists( 'current_category', $args ) || $args['current_category'] ) {
            if ( is_category() || is_tax() || is_tag() ) {
                if ( ! array_key_exists( 'taxonomy', $args ) ) {
                    $args['taxonomy'] = 'category';
                }
                $current_term_object = get_queried_object();
                if ( $args['taxonomy'] !== $current_term_object->taxonomy ) {
                    $output = str_replace( "class='cat-item-all'", "class='cat-item-all current-cat'", $output );
                }
            } else {
                $output = str_replace( "class='cat-item-all'", "class='cat-item-all current-cat'", $output );
            }
        }
    }

    return $output;
}
add_filter( 'wp_list_categories', 'wp_list_categories_highlight_all', 10, 2 );


function get_oldest_post_year() {
	global $wpdb;
	$oldest_post_id = $wpdb->get_row("SELECT post_date FROM {$wpdb->posts} WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date ASC");
	return get_the_date('Y', $oldest_post_id);
}

function my_customize_rest_cors() {
	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
	add_filter( 'rest_pre_serve_request', function( $value ) {
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Access-Control-Allow-Methods: GET' );
		header( 'Access-Control-Allow-Credentials: true' );
		header( 'Access-Control-Expose-Headers: Link', false );
		header( 'Access-Control-Allow-Headers: X-Requested-With' );
		
		return $value;
	} );
}
add_action( 'rest_api_init', 'my_customize_rest_cors', 15 );

add_filter( 'acf/fields/wysiwyg/toolbars', function ( $toolbars ) {
  // $toolbars['Custom'][1] = [ 'bold', 'italic', 'underline', 'forecolor', 'backcolor', 'bullist', 'link', 'unlink' ];
  $toolbars['Simple'][1] = [ 'bold', 'italic', 'underline', 'link', 'unlink', 'bullist' ];
	return $toolbars;
});