// wait until DOM is ready
document.addEventListener("DOMContentLoaded", function(event){
  
  console.log("DOM loaded");
  
  var pageHolder = document.getElementById('page-holder');
  var pageIn = document.getElementById('page-in');
  var pageOut = document.getElementById('page-out');
  
  /* TimeLine Page Transition In */
  /* ----------------------------------------- */
    var tlPageIn = gsap.timeline({
      paused: true,
      onComplete: function(){        
        pageHolder.remove();
        pageIn.remove();
        AOS.init({ duration: 1500, once: 'true' });
      }
    })
      .to(pageIn, { duration: .5, y: '0%' })
      .to(pageHolder, { alpha: 0 })
      .to(pageIn, { duration: .5, alpha: 0 });
  /* ----------------------------------------- TimeLine Page Transition In */
  
  /* Timeline Page Transition Out */
  /* ----------------------------------------- */
  var tlPageOut = gsap.timeline({ paused: true })
    .to(pageOut, { duration: .5, y: '0%' });
  /* ----------------------------------------- Timeline Page Transition Out */
  

  //wait until images, links, fonts, stylesheets, and js is loaded
  window.addEventListener("load", function(e){    
    tlPageIn.play();
  }, false);
  

  $('a').click(function(e) {
    e.preventDefault();    
    var targetAnchor = $(this).attr('href');
    tlPageOut.play();
    setTimeout(function(){
      window.location = targetAnchor;
    }, tlPageOut.duration());
  })

});