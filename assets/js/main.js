// import AOS from 'aos';
// align images out container single post
(function ($, root, undefined) {
  $(function () {
    "use strict";
    
    $('.projectAccordion .more').on('click', function(ev) {
      ev.preventDefault();

      let blocks = $(this).parent().parent().parent().find('.block');
      blocks.each(function( index ) {
        if ( index > 0 ) {
          $(this).toggleClass("active")
        }
      });

      $(this).toggleClass('active');
      if ( $(this).hasClass('active') ) {
        $(this).text('- Less');
      } else {
        $(this).text('+ More');
      }
    });

    $(document).ready(function () {
      // let fullImage = null;

      // arrows banner 
      let bannerThumb = document.querySelector('.js-slideshow__mediawrapper');
      if ( bannerThumb != null ) {
        let bannerThumbHeight = bannerThumb.offsetHeight;
        console.log(bannerThumbHeight);
        let bannerSlickArrow = document.querySelectorAll('.banner .slick-arrow');
        bannerSlickArrow.forEach(function(elemento){
          elemento.style.height = bannerThumbHeight + "px";
        });
      }
      
      // fixed header
      $(window).scroll(function() {
        const header = document.getElementById('header');
        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
          header.classList.add('top-0');
          header.classList.add('sticky');
          // if ( fullImage != null ) {
          //   fullImage.classList.remove('fix');
          // }
        } else {
          header.classList.remove('sticky');
          // header.classList.remove('shadow-lg');
        }
      });

      // show search
      const btnShowNews = document.getElementById('js-show-news');
      const boxNews = document.getElementById('js-news-content');
      if (btnShowNews) {
        btnShowNews.addEventListener('click', function (e) {
          boxNews.classList.toggle('active');
          setTimeout(() => {
            const scroll = document.body.scrollHeight;
            window.scrollTo({
              top: scroll,
              behavior: 'smooth'
            })
          }, 250);
          e.preventDefault();
        });        
      }

      // toggle nav
      $('.hamburguer').click(function () {
        $(this).toggleClass('active');
        $('.menu').toggleClass('active');
        $('.menu__line--one').toggleClass('rotate')
        $('.menu__line--two').toggleClass('rotate')
      });

      // animation on single project
      // let singleProject = document.getElementById("contentSingleProject");
      // if ( singleProject != null ) {
      //   let fullImage = singleProject.firstElementChild;
      //   if ( fullImage.classList.contains('full-image') ) {
      //     fullImage.classList.add('fix');

      //     $(window).scroll(function() {    
      //       var scroll = $(window).scrollTop();
      //       if (scroll >= 50) {
      //         fullImage.classList.remove('fix');
      //       }
      //     });
      //   } else {
      //     console.log('NÃO POSSUI');
      //   }
      // }
    });


    /* _slideshow */
    /* ----------------------------------------- */
      var slickHome = $('#js-slideshow-home');

      slickHome.on('afterChange', function(){
        updateSlickDotsAndArrowsPositionBasedOnMedia('afterChange')
      }).on('init', function(){
        updateSlickDotsAndArrowsPositionBasedOnMedia('init')
      }).on('setPosition', function(){        
        updateSlickDotsAndArrowsPositionBasedOnMedia('setPosition')
      }).slick({
        dots: true,
        arrows: true,
        infinite: true,
        speed: 750,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 3500,
        adaptiveHeight: true,
        fade: true,
        accessibility: false,
        prevArrow: '<button type="button" data-magicmouse-hover-class="size-120 bg-white slider-arrow" data-magicmouse-text="←" class="slick-prev">Previous</button>',
        nextArrow: '<button type="button" data-magicmouse-hover-class="size-120 bg-white slider-arrow" data-magicmouse-text="→" class="slick-next">Next</button>',
      });

      function updateSlickDotsAndArrowsPositionBasedOnMedia() {
        const slideMediaWrapper = slickHome.find('.slick-current .js-slideshow__mediawrapper');        
        const slickDotsWrapperEl = slickHome.find('.slick-dots');
        const slickArrows = slickHome.find('.slick-arrow');
        if (slideMediaWrapper[0] && slickDotsWrapperEl) {          
          slickDotsWrapperEl.css('top', slideMediaWrapper.height() + 21);
        }
        if (slideMediaWrapper[0] && slickArrows) {
          slickArrows.css('height', slideMediaWrapper.height());
        }
      }
    /* ----------------------------------------- _slideshow */
    

    // banner
    $(".slider-products-gallery").slick({
      dots: true,
      arrows: true,
      infinite: true,
      speed: 750,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 3500,
      fade: true
    });

    $(".slidepage").slick({
      dots: true,
      arrows: true,
      infinite: true,
      speed: 750,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 3500,
      fade: true,
      accessibility: false
    });

    // slider products control
    var $sliderProducts = $('.slider-products');

    if ( $sliderProducts.length ) {
      var currentSlide;
      var slidesCount;
      var sliderCounter = document.createElement('div');
      sliderCounter.classList.add('counter');
      
      var updateSliderCounter = function(slick, currentIndex) {
        currentSlide = addZero(slick.slickCurrentSlide() + 1);
        slidesCount = addZero(slick.slideCount);
        $(sliderCounter).text(currentSlide + '/' +slidesCount)
      };
    
      $sliderProducts.on('init', function(event, slick) {
        $sliderProducts.append(sliderCounter);
        updateSliderCounter(slick);
      });
    
      $sliderProducts.on('afterChange', function(event, slick, currentSlide) {
        updateSliderCounter(slick, currentSlide);
      });
    
      $sliderProducts.slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 750,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        fade: true
      });
    }

    function addZero(n) {
      return (n < 10) ? '0' + n : '' + n;
    }

    // add plus and minus
		$(document).on('click', '.btn-plus, .btn-minus', function (e) {
      var $qty = $(this).closest('.quantity').find('.qty'),
          currentVal = parseFloat($qty.val()),
          max = parseFloat($qty.attr('max')),
          min = parseFloat($qty.attr('min')),
          step = $qty.attr('step');
      
      if (!currentVal || currentVal === '' || currentVal === 'NaN') currentVal = 0;
      if (max === '' || max === 'NaN') max = '';
      if (min === '' || min === 'NaN') min = 0;
      if (step === 'any' || step === '' || step === undefined || parseFloat(step) === 'NaN') step = 1;
      
      if ($(this).is('.btn-plus')) {
        if (max && ( max == currentVal || currentVal > max )) {
          $qty.val(max);
        } else {
          $qty.val(currentVal + parseFloat(step));
        }
      } else {
        if (min && ( min == currentVal || currentVal < min )) {
          $qty.val(min);
        } else if (currentVal > 0) {
          $qty.val(currentVal - parseFloat(step));
        }
      }
      
      $qty.trigger('change');
      e.preventDefault();
    });

    // $('.wpcf7-form input[type="submit"]').replaceWith(
    //   '<button id="submit" type="submit" class="btn icon">' +
    //     $('.wpcf7-form input[type="submit"]').val() +
    //     "</button>"
    // );

    // Mascara de DDD e 9 dígitos para telefones
    var SPMaskBehavior = function (val) {
      return val.replace(/\D/g, "").length === 11
        ? "(00) 00000-0000"
        : "(00) 0000-00009";
    },
    spOptions = {
      onKeyPress: function (val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
    };
    $('.mask-phone, input[type="tel"]').mask(SPMaskBehavior, spOptions);
    $('.phone_with_ddd').mask('+99 99 9999-9999');

    // SELECT , caso queira excluir algum elemento, colocar 'select:not(elementos)'
    $("select")
      .not(".no-box")
      .not(".multiple")
      .not(".woo-variation-raw-select")
      .not(".woocommerce select")
      .wrap('<div class="select-box"></div>');

    // Fancybox
    $(
      "a[href$='.jpg'], a[href$='.png'], a[href$='.jpeg'], a[href$='.gif'], .fancybox"
    ).fancybox({
      loop: false,
    });

    // Rolagem suave
    $("a.smoothscroll").click(function () {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          $("html,body").animate({ scrollTop: target.offset().top }, 1000);
          return false;
        }
      }
    });

    // const options = {
    //   "hoverEffect": "circle-move",
    //   "hoverItemMove": false,
    //   "defaultCursor": false,
    //   };
    // magicMouse(options);
      
  });
})(jQuery, this);
