<?php
  _partials('_header-notes');
  
  $oldest_post_year = get_oldest_post_year();
  $post_class = get_post_class();
  
  for ($year=date('Y'); $year >= $oldest_post_year ; $year--) { 

      $args = ['post_type' => 'post', 'posts_per_page' => -1, 'date_query' => [ 'year' => $year ] ];
      $query = new WP_Query($args);
      if ($query->have_posts()):
        
        
        if ($year == date('Y')) {
          
          $year_classes = array_merge($post_class, ['year-current', 'year-' . $year, 'flex']);
          echo '<div class="' . esc_attr( implode( ' ', $year_classes ) ) . '" >';
            while ($query->have_posts()): $query->the_post();
              _partials('_teaser-notes-thumb');
            endwhile;
          echo '</div>';

        } else if ($year - date('Y') == -1) {
          $year_classes = array_merge($post_class, ['year-' . $year, 'flex', 'border-t-2 border-black']);
          echo '<div class="' . esc_attr( implode( ' ', $year_classes ) ) . '" >';
            echo '<h2 class="text-3xl md:text-4xl lg:text-5xl xl:text-6xl 2xl:text-7xl 4k:text-8xl w-100 shrink-0">'.$year.'</h2>';
            while ($query->have_posts()): $query->the_post();
              _partials('_teaser-notes-thumb');
            endwhile;
          echo '</div>';
            
        } else {
            $year_classes = array_merge($post_class, ['year-' . $year, 'flex flex-wrap', 'border-t-2 border-black pt-10']);
            echo '<div class="' . esc_attr( implode( ' ', $year_classes ) ) . '" >';
              echo '<h2 class="text-5xl md:text-5xl lg:text-5xl xl:text-6xl 2xl:text-7xl 4k:text-8xl border-top w-full md:w-1/2 shrink-0">'.$year.'</h2>';
              echo '<div class="w-full md:w-1/2 shrink-0">';
                while ($query->have_posts()): $query->the_post();
                _partials('_teaser-notes-nothumb');
              endwhile;
              echo '</div>';
            echo '</div>';            
        }
      endif; 
      wp_reset_postdata();
  } // end for

  _partials('_end');
  get_footer();