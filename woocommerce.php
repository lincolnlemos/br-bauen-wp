<?php
  get_header();
  _partials('_start');
?>
  <?php if ( is_shop() || is_product_category() ) : ?>
    <div class="flex flex-col xl:flex-row items-start mt-16 md:mt-36 mb-16 md:mb-24 pb-3 px-2.5">
      <div class="w-full xl:w-6/12" data-aos="fade-up">
        <?php
          $title = ( get_field('custom_title', get_option( 'woocommerce_shop_page_id' )) ) ? get_field('custom_title', get_option( 'woocommerce_shop_page_id' )) : get_the_title();
          echo '<h2 class="text-4xl xl:text-6xl 2xl:text-8xl text-black">'. esc_html( $title ) .'</h2>';
        ?>
      </div>

      <div class="w-full xl:w-6/12 flex flex-col md:flex-row items-start justify-start mt-10 md:mt-0" data-aos="fade-up" data-aos-delay="300">
        <div class="flex-shrink-0 mb-2 md:mb-0">
          <p class="text-xl">Filter by:</p>
        </div>
        <div class="flex-grow md:ml-20">
          <ul class="nav-theme-anchor nav-theme-anchor-clean nav-theme-anchor-gray text-xl text-black">
            <?php
              wp_list_categories([
                'title_li'        => '',
                'show_option_all' => __( 'All', 'brbauen' ),
                'taxonomy'        => 'product_cat',
              ]);
            ?> 
          </ul>
        </div>
      </div>
    </div>  
  <?php endif; ?>

  <?php if ( is_shop() ) : ?>
    <div class="w-full">
      <?php
        $loop = new WP_Query([
          'post_type'      => 'product', 
          'posts_per_page' => -1,
          'tax_query' => [
            [
              'taxonomy' => 'product_visibility',
              'field'    => 'name',
              'terms'    => 'featured',
              'operator' => 'IN',
            ]
          ]
        ]);
        if ( $loop->have_posts() ) :
          echo '<div class="slider-products mb-2.5">';
            while ( $loop->have_posts() ) : $loop->the_post();
              echo '<div>';
                wc_get_template_part( 'content', 'featured' );
              echo '</div>';
            endwhile;
          echo '</div>';
        wp_reset_postdata();
        endif;
      ?>
    </div>
  <?php endif; ?>

  <div class="w-full mb-20 <?php echo ( is_product() ) ? 'md:mb-40' : 'md:mb-72'; ?>">
    <?php woocommerce_content(); ?>
  </div>
<?php
  if ( is_shop() || is_product_category() ) {
    echo '<a class="text-4xl xl:text-6xl 2xl:text-8xl hover-line 2xl:hover-line-6x" href="'.get_permalink(7).'">'. __('Back to portfolio', 'brbauen') .'<br />←</a>';
  } else if ( is_product() ) {
    echo '<a class="text-4xl xl:text-6xl 2xl:text-8xl hover-line 2xl:hover-line-6x" href="'.wc_get_page_permalink('shop').'">'. __('Back to the store', 'brbauen') .'<br />←</a>';
  }
  _partials('_end');
  get_footer();