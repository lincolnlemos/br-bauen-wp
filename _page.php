<?php
  get_header();
  _partials('_start');
  if ( have_posts() ) while ( have_posts() ) : the_post();
?>
  
  <article <?php post_class( 'page' ); ?> >
    <div class="content"><?php the_content(); ?></div>
    <?php if ( get_field('page_block') ) { ?>
      <div>
        <?php
          foreach ( get_field('page_block') as $item) {
            global $item;
            _partials('_'.$item['acf_fc_layout']);
          }
        ?>
      </div>
    <?php }; ?>
  </article>

<?php
  endwhile;
  _partials('_lets-work-together');
  _partials('_end');
  get_footer();