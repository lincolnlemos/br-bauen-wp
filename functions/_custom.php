<?php
  // suporte of title
  add_theme_support( 'title-tag' );
  add_filter( 'get_user_option_admin_color','bsAdminColor' );
  add_filter( 'embed_oembed_html', 'wrapEmbedWithDiv', 10, 3 );

  function _partials($file) { include BS_PARTIALS_PATH . $file.'.php'; }
  function _loop($file) { include BS_LOOP_PATH . $file.'.php'; }
  function images_url($file) { echo get_stylesheet_directory_uri() . '/assets/images/'. $file; }
  function get_images_url($file) { return get_stylesheet_directory_uri() . '/assets/images/'. $file; }
  function bsAdminColor() { return 'modern'; }
  function wrapEmbedWithDiv($html, $url, $attr) { return "<div class=\"embed-container\">".$html."</div>"; }

  function bsAcfInit() {
    acf_update_setting('google_api_key', 'AIzaSyDScik_SlFbMa3LF2yrdlKNOeFFwGLjJRc');
  }
  add_action('acf/init', 'bsAcfInit');
  
  // custom excerpt
  function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if ( count($content)>=$limit ) :
      array_pop($content);
      $content = implode(" ",$content).'...';
    else : $content = implode(" ",$content);
    endif;
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return strip_tags($content);
  }

  // Custom Excerpt function for Advanced Custom Fields
  function wp_trim_excerpt_modified($text, $content_length = 55, $remove_breaks = false) {
    if ( '' != $text ) {
      $text = strip_shortcodes( $text );
      $text = excerpt_remove_blocks( $text );
      $text = apply_filters( 'the_content', $text );
      $text = str_replace(']]>', ']]&gt;', $text);
      $num_words = $content_length;
      $original_text = $text;
      $text = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $text );

      // Here is our modification
      // Allow <p> and <strong>
      $text = strip_tags($text);

      if ( $remove_breaks )
        $text = preg_replace('/[\r\n\t ]+/', ' ', $text);
      $text = trim( $text );
      if ( strpos( _x( 'words', 'Word count type. Do not translate!' ), 'characters' ) === 0 && preg_match( '/^utf\-?8$/i', get_option( 'blog_charset' ) ) ) {
        $text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
        preg_match_all( '/./u', $text, $words_array );
        $words_array = array_slice( $words_array[0], 0, $num_words + 1 );
        $sep = '';
      } else {
        $words_array = preg_split( "/[\n\r\t ]+/", $text, $num_words + 1, PREG_SPLIT_NO_EMPTY );
        $sep = ' ';
      }
      if ( count( $words_array ) > $num_words ) {
        array_pop( $words_array );
        $text = implode( $sep, $words_array );
      } else {
        $text = implode( $sep, $words_array );
      }
    }
    return $text;
  }

  // add current class in nav custom post type "project"
  add_filter('nav_menu_css_class', 'add_additional_class', 1, 3);
  add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
  function add_additional_class($classes, $item, $args){
    if(isset($args->add_li_class)){
      $classes[] = $args->add_li_class;
    }
    return $classes;
  }
  function current_type_nav_class($classes, $item) {
    $post_type = get_query_var('post_type');

    if ( get_post_type() == $post_type )
        $classes = array_filter($classes, "get_current_value" );

    if( in_array( $post_type.'-menu-item', $classes ) )
        array_push($classes, 'current_page_parent');

    return $classes;
  }
  function get_current_value( $element ) {
    return ( $element != "current_page_parent" );
  }

  // remove content box to custom post
  add_action( 'init', function() {
    remove_post_type_support( 'projeto', 'editor' );
  }, 99);

  // function my_custom_status_creation(){
  //   register_post_status( 'coming-soon', array(
  //     'label'                     => _x( 'Coming soon', 'post' ),
  //     'label_count'               => _n_noop( 'Coming soon <span class="count">(%s)</span>', 'Coming soon <span class="count">(%s)</span>'),
  //     'public'                    => true,
  //     'exclude_from_search'       => false,
  //     'show_in_admin_all_list'    => true,
  //     'show_in_admin_status_list' => true
  //   ));
  // }
  // add_action( 'init', 'my_custom_status_creation' );
  
  // function add_to_post_status_dropdown() {
  //   global $post;
  //   if($post->post_type != 'projeto')
  //     return false;

  //   $status = ($post->post_status == 'coming-soon') ? "jQuery( '#post-status-display' ).text( 'Coming soon' ); jQuery( 
  //   'select[name=\"post_status\"]' ).val('coming-soon');" : '';
  //   echo "<script>
  //   jQuery(document).ready( function() {
  //   jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"coming-sson\">Coming soon</option>' );
  //   ".$status."
  //   });
  //   </script>";
  // }
  // add_action( 'post_submitbox_misc_actions', 'add_to_post_status_dropdown');

  // function display_archive_state( $states ) {
  //   global $post;
  //   $arg = get_query_var( 'post_status' );
  //   if($arg != 'coming-soon'){
  //     if($post->post_status == 'coming-soon'){
  //       echo "<script>
  //         jQuery(document).ready( function() {
  //           jQuery( '#post-status-display' ).text( 'Coming soon' );
  //         });
  //       </script>";
  //       return array('Coming soon');
  //     }
  //   }
  //   return $states;
  // }
  // add_filter( 'display_post_states', 'display_archive_state' );

  // Registering custom post status
  function wpb_custom_post_status(){
    register_post_status('coming', array(
      'label'                     => _x( 'Coming soon', 'post' ),
      'public'                    => true,
      'exclude_from_search'       => false,
      'show_in_admin_all_list'    => true,
      'show_in_admin_status_list' => true,
      'label_count'               => _n_noop( 'Coming soon <span class="count">(%s)</span>', 'Coming soon <span class="count">(%s)</span>' ),
    ) );
  }
  add_action( 'init', 'wpb_custom_post_status' );

  // Using jQuery to add it to post status dropdown
  add_action('admin_footer-post.php', 'wpb_append_post_status_list');
  function wpb_append_post_status_list(){
    global $post;
    $complete = '';
    $label = '';
    if($post->post_type == 'projeto'){
      if($post->post_status == 'coming'){
        $complete = ' selected="selected"';
        $label = '<span id="post-status-display"> Coming soon</span>';
      }
      echo '
        <script>
          jQuery(document).ready(function($){
            $("select#post_status").append("<option value=\"coming\" '.$complete.'>Coming soon</option>");
            $(".misc-pub-section label").append("'.$label.'");
          });
        </script>
      ';
    }
  }

  // Using jQuery to add it to post status quick edit
  add_action('admin_footer-edit.php','rudr_status_into_inline_edit');
  function rudr_status_into_inline_edit() { // ultra-simple example
    echo "<script>
    jQuery(document).ready( function() {
      jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"coming\">Coming soon</option>' );
    });
    </script>";
  }

  // insert label post status on admin page
  // add_filter( 'display_post_states', 'rudr_display_status_label' );
  function rudr_display_status_label( $statuses ) {
    global $post; // we need it to check current post status
    if( get_query_var( 'post_status' ) != 'coming' ){ // not for pages with all posts of this status
      if( $post->post_status == 'coming' ){ // если статус поста - Архив
        return array('Coming soon'); // returning our status label
      }
    }
    return $statuses; // returning the array with default statuses
  }