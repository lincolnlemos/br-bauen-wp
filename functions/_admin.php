<?php
  // Filter Yoast Meta Priority
  add_filter( 'wpseo_metabox_prio', function() { return 'low';});

  // Custom WP Login
  add_action( 'login_head', 'bsdevCustomLogin' );
  add_filter( 'login_headerurl', 'bsdevCustomLoginUrl' );
  add_filter( 'login_headertitle', 'bsdevCustomLoginTitle' );
  function bsdevCustomLogin() {
    echo '<link media="all" type="text/css" href="'.get_template_directory_uri().'/assets/css/login-style.css" rel="stylesheet">';
    $bg = (get_field( 'logo_dark', 'option' )) ? get_field( 'logo_dark', 'option' ) : 'https://upcode.cloud/signature/logotipo.svg' ;
    ?>
      <style type="text/css" media="screen">
        body.login h1 a {
          background-image: url(<?php echo $bg; ?>);
          background-size: contain;
          background-position: center center;
        }
      </style>
    <?php
  }
  function bsdevCustomLoginUrl() { return home_url(); }
  function bsdevCustomLoginTitle() { return get_option('blogname'); }

  // Disables standard dashboard widgets
  add_action( 'admin_menu', 'disableDefaultDashboardWidgets' );
  function disableDefaultDashboardWidgets() {
    remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );
  }

  // Load CSS files into Admin
  add_action( 'admin_enqueue_scripts', 'loadCustomAdminCSS' );
  function loadCustomAdminCSS() {
    wp_register_style( 'custom_wp_admin_css', get_bloginfo( 'stylesheet_directory' ) . '/assets/css/admin-style.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
  }

  // User ID on body-class
  add_filter( 'admin_body_class', 'idUserBodyClass' );
  function idUserBodyClass( $classes ) {
    global $current_user;
    $classes .= ' user-' . $current_user->ID;
    return trim( $classes );
  }

  // enable svg support
  add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
    $filetype = wp_check_filetype( $filename, $mimes );
    return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
    ];
  }, 10, 4 );

  function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter( 'upload_mimes', 'cc_mime_types' );

  function fix_svg() {
    echo '<style type="text/css">
      .attachment-266x266, .thumbnail img {
        width: 100% !important;
        height: auto !important;
      }
    </style>';
  }
  add_action( 'admin_head', 'fix_svg' );

  function custom_preview_post_link($permalink, $post) {
    $base_permalink = str_replace(home_url('/'), '', $permalink);
    
    $preview_url = (get_field('preview_url', 'option')) ? get_field('preview_url', 'option') : home_url('/');
    $link = $preview_url . $base_permalink;
    
    $link = remove_query_arg('preview_id', $link);
    $link = remove_query_arg('preview_nonce', $link);
    $link = remove_query_arg('_thumbnail_id', $link);

    $new_permalink = add_query_arg(['preview' => 'true'], $link);
    $new_permalink = wp_sanitize_redirect($new_permalink);

    return esc_url($new_permalink);
  }
  add_filter('preview_post_link', 'custom_preview_post_link', 10, 2);
  add_filter('preview_page_link', 'custom_preview_post_link', 10, 2);

  // add_filter('preview_post_link', 'custom_remove_preview_button');
  // add_filter('preview_page_link', 'custom_remove_preview_button');
  function custom_remove_preview_button($link) {
    return '';
  }

  // add_action('post_submitbox_misc_actions', 'add_custom_button_preview');
  function add_custom_button_preview() {
    global $post;

    if (is_admin()) {
      $permalink = get_permalink($post->ID);
      $base_permalink = str_replace(home_url('/'), '', $permalink);

      $preview_url = (get_field('preview_url', 'option')) ? get_field('preview_url', 'option') : home_url('/');
      $link = $preview_url . $base_permalink;
      $new_permalink = add_query_arg('preview', 'true', $link);


      echo '<div class="misc-pub-section custom-button-section">';
        echo '<a href="'.$new_permalink.'" id="custom-button" class="button" target="_blank">Preview</a>';
      echo '</div>';
    }
  }


  function add_custom_build_button() {
    add_submenu_page(
      'tools.php', 
      'Fazer Build no Vercel', 
      'Build', 
      'manage_options', 
      'custom-build-page',
      'custom_build_page_html'
    );
  }

  function custom_build_page_html() {
  ?>
    <div class="wrap">
      <h1 class="wp-heading-inline" style="margin-bottom:20px;">Fazer Build no Vercel</h1>
      <!-- <p>O processo de build leva de 2 à 3 minutos.</p> -->

      <hr class="wp-header-end">

      <form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
        <input type="hidden" name="action" value="trigger_build">
        <input type="submit" class="button button-primary" value="Fazer build">
      </form>
    </div>

    
    <?php
  }
  add_action('admin_menu', 'add_custom_build_button');