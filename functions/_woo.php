<?php
// check if woocommerce is active
function brbauen_is_woocommerce_activated() {
  return class_exists( 'woocommerce' ) ? true : false;
}
// declare support
function bf_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'bf_add_woocommerce_support', 100 );

// dequeue native styles and enqueue styles of theme
function brbauen_dequeue_styles( $enqueue_styles ) {
  unset( $enqueue_styles['woocommerce-general'] );
  unset( $enqueue_styles['woocommerce-layout'] );
  wp_dequeue_style('select2');
  
  return $enqueue_styles;
}
add_filter( 'woocommerce_enqueue_styles', 'brbauen_dequeue_styles' );

// remove css libraries
function brbauen_remove_css() {
	wp_dequeue_style( 'woo-variation-swatches' );
	wp_dequeue_style( 'pi-dcw' );
}
add_action( 'wp_enqueue_scripts', 'brbauen_remove_css', 100 );

// custom filters and actions
add_filter( 'woocommerce_add_to_cart_fragments', 'brbauen_cart_link_fragment' );
add_filter( 'woocommerce_show_page_title', '__return_false' );
add_filter( 'single_product_archive_thumbnail_size', function( $size ) {
  return 'full';
} );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_show_product_loop_sale_flash', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 10 );

add_filter( 'loop_shop_columns', 'custom_loop_columns', 999 );
if (!function_exists('custom_loop_columns')) {
	function custom_loop_columns() {
		return 2;
	}
}

add_filter( 'woocommerce_output_related_products_args', 'brbauen_related_products_args', 20 );
  function brbauen_related_products_args( $args ) {
	$args['posts_per_page'] = 4;
	$args['columns']        = 4;
	return $args;
}

function custom_products_query( $query ) {
  if ( is_product_category() ) return;
  $query->set( 'tax_query', [
    [
      'taxonomy' => 'product_visibility',
      'field'    => 'name',
      'terms'    => 'featured',
      'operator' => 'NOT IN',
    ]
  ] );
}
add_action( 'woocommerce_product_query', 'custom_products_query' );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'brbauen_template_loop_product_thumbnail', 10 );
function brbauen_template_loop_product_thumbnail() {
  echo '<div class="img-wrapper ratio">';
    echo woocommerce_get_product_thumbnail();
  echo '</div>';
}

// open content-wrapper
add_action( 'woocommerce_shop_loop_item_title', 'brbauen_template_loop_product_content_start', 5 );
function brbauen_template_loop_product_content_start() {
  echo '<div class="content-wrapper flex flex-col items-start p-5 md:p-7">';
}

add_action( 'woocommerce_after_shop_loop_item_title', 'custom_before_title_description', 5 );
function custom_before_title_description() {
  global $product;

  if ( is_shop() || is_product_category() && $product->get_short_description() ) {
    echo '<p>'. esc_html( $product->get_short_description() ) .'</p>';
  }
}

add_action( 'woocommerce_after_shop_loop_item', 'brbauen_more_details_button', 10 );
function brbauen_more_details_button() {
  echo '<a href="'.get_permalink().'" class="more-details text-white tracking-tightest">'. __('+ More Details', 'brbauen') .'</a>';
}

add_action( 'woocommerce_single_product_summary', 'brbauen_product_informations_summary', 20 );
function brbauen_product_informations_summary() {

  $informations = get_field('product_infos');

  if ( !empty($informations) ) :
    echo '<div class="accordion flex flex-col flex-wrap w-full xl:w-1/2">';
      foreach ($informations as $information) :
        $item = '<div class="accordion-item w-full cursor-pointer relative mb-8">';
          $item .= '<p class="accordion-title font-black text-xl">'.$information['title'].'</p>';

          $item .= '<div class="accordion-content text-xl">';
            $item .= $information['content'];
          $item .= '</div>';
        $item .= '</div>';

        echo $item;
      endforeach;
    echo '</div>';
  endif;
}

// add_filter('woocommerce_currency_symbol', 'brbauen_currency_symbol', 10, 2);
function brbauen_currency_symbol( $currency_symbol, $currency ) {
  switch( $currency ) {
    case 'USD': $currency_symbol = 'usd'; break;
  }
  return $currency_symbol;
}

// close content-wrapper
add_action( 'woocommerce_after_shop_loop_item', 'brbauen_template_loop_product_content_end', 15 );
function brbauen_template_loop_product_content_end() {
  echo '</div>';
}

// change add to cart text on single product page
add_filter( 'woocommerce_product_single_add_to_cart_text', 'brbauen_add_to_cart_button_text_single' ); 
function brbauen_add_to_cart_button_text_single() {
  return __( '→ Add to Cart', 'brbauen' ); 
}

// change add to cart text on product archives page
add_filter( 'woocommerce_product_add_to_cart_text', 'brbauen_add_to_cart_button_text_archives' );  
function brbauen_add_to_cart_button_text_archives() {
  return __( '↳ Add to Cart', 'brbauen' );
}

// change related products title
add_filter( 'woocommerce_product_related_products_heading', 'misha_change_related_products_heading' );
function misha_change_related_products_heading() {
	return __( 'Recommended', 'brbauen' );
}

// woocommerce mini cart
function brbauen_cart_link_fragment( $fragments ) {
  global $woocommerce;

  ob_start();
  brbauen_cart_link();
  $fragments['a.cart-contents'] = ob_get_clean();

  ob_start();
  brbauen_handheld_footer_bar_cart_link();
  $fragments['a.footer-cart-contents'] = ob_get_clean();

  return $fragments;
}

function brbauen_cart_link() {
  if ( ! brbauen_woo_cart_available() ) {
    return;
  }
  ?>
    <a class="cart-contents hover-line" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'brbauen' ); ?>">
      <?php /* translators: %d: number of items in cart */ ?>
      Cart <span class="count">(<?php echo wp_kses_data( sprintf( _n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'brbauen' ), WC()->cart->get_cart_contents_count() ) ); ?>)</span>
    </a>
  <?php
}

function brbauen_woo_cart_available() {
  $woo = WC();
  return $woo instanceof \WooCommerce && $woo->cart instanceof \WC_Cart;
}

function brbauen_handheld_footer_bar_cart_link() {
  if ( ! brbauen_woo_cart_available() ) {
    return;
  }
  ?>
    <a class="footer-cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>"><?php esc_html_e( 'Cart', 'brbauen' ); ?>
      <span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ); ?></span>
    </a>
  <?php
}

function brbauen_header_cart() {
  if ( brbauen_is_woocommerce_activated() ) {
    $class = ( is_cart() ) ? 'current-menu-item' : '';
    ?>
      <ul id="site-header-cart" class="site-header-cart menu ml-10 xl:ml-20">
        <li class="flex items-center <?php echo esc_attr( $class ); ?>">
          <i class="icon mr-1">
            <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M22 16.359C15.3053 16.359 4.35715 16.359 4.35715 16.359V11.5M0 2H4.3572V4.05128M4.3572 4.05128H20.5477L20.5476 11.5H4.35715M4.3572 4.05128L4.35715 11.5" stroke="black" stroke-width="3"/>
              <circle cx="16" cy="21" r="2" fill="black"/>
              <circle cx="9" cy="21" r="2" fill="black"/>
            </svg>
          </i> 
          <?php brbauen_cart_link(); ?>
        </li>
      </ul>
    <?php
  }
}

// add_action( 'woocommerce_sale_flash', 'sale_badge_percentage', 25 );
function sale_badge_percentage() {
  global $product;

  if ( ! $product->is_on_sale() ) return;

  if ( $product->is_type( 'simple' ) ) :
    $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;

  elseif ( $product->is_type( 'variable' ) ) :
    $max_percentage = 0;

    foreach ( $product->get_children() as $child_id ) :
      $variation = wc_get_product( $child_id );
      $price     = $variation->get_regular_price();
      $sale      = $variation->get_sale_price();

      if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
      if ( $percentage > $max_percentage ) {
        $max_percentage = $percentage;
      }
    endforeach;
  endif;

  if ( $max_percentage > 0 ) echo "<span class='onsale'>-" . round($max_percentage) . "%</span>";
}

// add_action( 'woocommerce_before_shop_loop_item_title', function() {
//   echo '<figure class="rounded-3">';
// }, 9 );

// add_action( 'woocommerce_before_shop_loop_item_title', function() {
//   echo '</figure>';
//   echo '<div class="content">';
// }, 11 );

// add_action( 'woocommerce_after_shop_loop_item_title', function() {
//   echo '</div>';
// }, 11 );

// remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
// remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

// tabs control
// remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
// add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );

// add_action('woocommerce_before_main_content', 'brbauen_wrapper_start', 10);
// add_action('woocommerce_after_main_content', 'brbauen_wrapper_end', 10);