<?php

add_action('rest_api_init', function () {

  register_rest_route( 'nuxt/v1', 'initial-data', [
    'methods'  => 'GET',
    'callback' => 'get_wp_initial_data'
  ]);
  
  register_rest_route( 'nuxt/v1', 'by-slug', [
    'methods'  => 'GET',
    'callback' => 'get_by_slug'
  ]);

  register_rest_route( 'nuxt/v1', 'by-id', [
    'methods'  => 'GET',
    'callback' => 'get_by_id'
  ]);

  register_rest_route('brbauen/v1', '/projects', [
    'methods' => 'GET',
    'callback' => 'api_get_projects',
  ]);

  register_rest_route( 'contact-form-7/v1', '/forms', [
    'methods'  => 'GET',
    'callback' => 'get_contact_form_7_forms',
  ]);

  register_rest_route('contact-form-7/v1', 'forms/(?P<id>[\d]+)', [
    'methods'  => 'GET',
    'callback' => 'get_contact_form_by_id'
  ]);

});

function update_post_type_rest_response($post_response, $post, $context) {
  $data = $post_response->data;

  $head_json = $data['yoast_head_json'];

  if (isset($data['yoast_head'])) {
    unset($data['yoast_head']);
  }
  if (isset($data['yoast_head_json'])) {
    unset($data['yoast_head_json']);
  }

  $post->author_name = (get_post_field( 'post_author', $post->ID)) ? get_the_author_meta('display_name', get_post_field( 'post_author', $post->ID)) : '';
  
  $post_response->data = [
    'pageTemplate'      => get_page_template_slug($post->ID),
    'postType'          => get_post_type($post->ID),
    'post'              => $post,
    'data'              => $data,
    'get_post_class'    => get_post_class($post->ID),
    'acf'               => get_fields($post->ID),
    'url'               => str_replace('pt-br/work/', 'pt-br/projetos/', get_permalink($post->ID)),
    'yoast_head_json'   => $head_json
  ];

  return $post_response;
}
add_filter('rest_prepare_post', 'update_post_type_rest_response', 12, 3);
add_filter('rest_prepare_projeto', 'update_post_type_rest_response', 12, 3);

function switch_lang_if_has_code($request){ 
  if ($request->get_param('lang_code')) {
    global $sitepress;
    $sitepress->switch_lang($request->get_param('lang_code'));
  }
}

function get_menus_in_wpml_language($lang) {
  $locations = get_nav_menu_locations();  
  $menus = [];
  foreach ($locations as $location => $menu_id) {
    $menus[$location] = [];
    $menu_id = apply_filters( 'wpml_object_id', $menu_id, 'nav_menu', FALSE, $lang['code'] );      
    $menus[$location] = wp_get_nav_menu_items($menu_id);    
  }
  return $menus;
}

function get_wp_initial_data() {  
  $initial_data = [];
  $default_lang = apply_filters('wpml_default_language', NULL );

  
  $languages = apply_filters( 'wpml_active_languages', NULL, array ('skip_missing' => 0 ) ) ;
  foreach ($languages as $code => $lang) {
    global $sitepress;
    $sitepress->switch_lang($code);
    $prefix = $code == $default_lang ? '' : $code . '_';
    
    $initial_data[$code]['wpml'] = $lang;
    $initial_data[$code]['options'] = [
      'logoUrl'     => get_field( $prefix . 'logo_dark', 'option' ),
      'siteName'    => get_bloginfo( $prefix . 'name', 'display' ),
      'email'       => get_field( $prefix . 'email_info', 'option' ),
      'phone'       => get_field( $prefix . 'phone_info', 'option' ),
      'facebook'    => get_field( $prefix . 'facebook', 'option' ),
      'instagram'   => get_field( $prefix . 'instagram', 'option' ),
      'behance'     => get_field( $prefix . 'behance', 'option' ),
      'address'     => get_field( $prefix . 'rpt_address', 'option' ),
      'privacyPage' => get_privacy_policy_url(),
      'allFields'   => get_fields('option')
    ];
    $initial_data[$code]['menus']           = get_menus_in_wpml_language($lang);
    $initial_data[$code]['frontPageId']     = get_option('page_on_front');
    $initial_data[$code]['frontPageUrl']    = get_permalink(get_option('page_on_front'));
    $initial_data[$code]['pageForPostsId']  = get_option('page_for_posts');
    $initial_data[$code]['pageForPostsUrl'] = get_permalink(get_option('page_for_posts'));
    $initial_data[$code]['workTerms']       = get_my_taxonomy_terms('categoria-projeto');
    $initial_data[$code]['noteTerms']       = get_my_taxonomy_terms('category');
  }
  
  $my_default_lang = apply_filters('wpml_default_language', NULL );  
  $sitepress->switch_lang($code);

  return wp_send_json($initial_data);
}

function get_by_slug($request) {
  global $sitepress;
  $slug         = $request->get_param('slug');
  $default_lang = apply_filters('wpml_default_language', NULL );  
  $lang         = $request->get_param('lang') ?? $default_lang;
  $sitepress->switch_lang($lang);
  
  // Handle if error.
  if ( empty( $slug ) ) {
    return 'Slug was not provided';
  }  
  
  global $wpdb;
  $query = $wpdb->prepare("SELECT {$wpdb->posts}.* FROM {$wpdb->posts} 
  JOIN {$wpdb->prefix}icl_translations ON {$wpdb->posts}.ID = {$wpdb->prefix}icl_translations.element_id
  WHERE {$wpdb->posts}.post_name = %s 
  AND {$wpdb->prefix}icl_translations.language_code = %s", $slug, $lang);
  $post = $wpdb->get_row($query);  

  if ($query) {
    return get_post_data($post, $request);
  } else {
    return 'Post not found';
  }

  return $slug;
}

function get_by_id($request) {
  $post_id = $request->get_param('id');

  // Handle if error.
  if ( empty( $post_id ) ) {    
    return 'ID was not provided';
  }

  global $wpdb;
  $post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE ID = %d", $post_id ) );
  
  if ($post) {
    return get_post_data($post, $request);
  } else {
    return 'error';
  }

  return $post_id;
}

function get_post_data($post, $apiRequest) {
  $isPreview = $apiRequest->get_param('preview');

  $post_type = get_post_type($post->ID);
  $taxonomy = '';

  if ($post_type === 'post') {
    $taxonomy = 'category';
  } elseif ($post_type === 'projeto') {
    $taxonomy = 'categoria-projeto';
  }

  $endpoint = ($isPreview) ? '/wp/v2/'.$post_type.'s/' . $post->ID . '/revisions' : '/wp/v2/'.$post_type.'s/' . $post->ID;

  authenticate_user_by_username('pp');

  // Create request from pages endpoint by frontpage id.
  $request = new \WP_REST_Request( 'GET', $endpoint);
  $request->set_query_params([ '_embed' => true ]);

  $post->author_name = (get_post_field( 'post_author', $post->ID)) ? get_the_author_meta('display_name', get_post_field( 'post_author', $post->ID)) : '';
  
  // Parse request to get data.
  $response         = rest_do_request( $request );
  $response_data    = $response->get_data();
  $post_revision_id = ($isPreview) ? $response_data[0]['id'] : $post->ID;

  $categories = get_the_terms($post->ID, $taxonomy);
  $response_data['categories'] = !empty($categories) ? $categories : [];

  $relateds = [];
  if (in_array($post_type, ['post', 'projeto']) && $taxonomy) {
    $relateds = get_relateds($post, $taxonomy, $post_type);
  }

  return [
    'wpml_urls'       => get_links_in_other_languages($post),
    'pageTemplate'    => get_page_template_slug($post->ID),
    'postType'        => get_post_type($post->ID),
    'post'            => $post,
    'data'            => $response_data,
    'get_post_class'  => get_post_class($post->ID),
    'acf'             => get_fields($post_revision_id),
    'relateds'        => $relateds
  ];
}

function get_links_in_other_languages($post) {
  $links = [];
  $languages = apply_filters( 'wpml_active_languages', NULL, array ('skip_missing' => 0 ) ) ;  

  foreach ($languages as $lang ) {
    $post_id_in_lang = apply_filters( 'wpml_object_id', $post->ID, $post->post_type, FALSE, $lang['code'] );
    // Added str_replace by lack of success on change the URL via WPML
    $links[$lang['code']] = str_replace('pt-br/work/', 'pt-br/projetos/', get_my_permalink($post->ID, $post_id_in_lang, $lang['code']));
  }
  return $links;
}

function get_relateds($post, $taxonomy = 'category', $post_type = 'post') {
  $current_language = apply_filters('wpml_current_language', null);
  $terms            = wp_get_post_terms($post->ID, $taxonomy);

  if (empty($terms)) {
    return [];
  }

  $args = [
    'post_type'    => $post_type,
    'post__not_in' => [$post->ID],
    'tax_query'    => [
      [
        'taxonomy' => $taxonomy,
        'field'    => 'id',
        'terms'    => wp_list_pluck($terms, 'term_id'),
        'operator' => 'IN',
      ],
    ],
    'posts_per_page' => 4,
    'orderby' => 'rand',
    'suppress_filters' => false,
    'lang' => $current_language,
  ];

  $related_posts = get_posts($args);

  foreach ($related_posts as &$related_post) {
    $categories = get_the_terms($related_post->ID, $taxonomy);

    $related_post->wpml_urls  = get_links_in_other_languages($related_post);
    $related_post->thumbnail  = get_the_post_thumbnail_url($related_post->ID, 'full');
    $related_post->categories = !empty($categories) ? $categories : [];
    $related_post->url        = str_replace('pt-br/work/', 'pt-br/projetos/', get_permalink($related_post->ID));
  }

  if ($post_type === 'projeto') {
    foreach ($related_posts as &$related_post) {
      $related_post->project_state = get_field('project_state', $related_post->ID);
    }
  }

  return $related_posts;
}

function api_get_projects() {
  $projects = [];
  $args = [
    'post_type'     => 'projeto',
    'post_per_page' => -1,
    'post_status'   => 'any',
  ];
  $loop = new WP_Query($args);
  while ($loop->have_posts()) : $loop->the_post();
    $id = get_the_ID();
    $slug = get_post_field('post_name', $id);
    $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'categoria-projeto' );
    foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
    $terms = implode(", ", $terms_name);
    $post = [
      'id'            => $id,
      'slug'          => $slug,
      'title'         => get_the_title( $id ),
      'status'        => get_post_status( $id ),
      'featured_url'  => get_the_post_thumbnail_url( $id, 'full' ),
      'terms'         => $terms,
      'link'          => get_the_permalink( $id ),
    ];

    $projects[$slug] = $post;
  endwhile;

  return rest_ensure_response($projects);
}

function get_contact_form_7_forms( $request ) {
  $forms   = WPCF7_ContactForm::find();
  $results = [];

  foreach ( $forms as $form ) {
    $mail_tags = $form->scan_form_tags( ['name-attr' => true] );

    $form_data = [
      'id'        => $form->id(),
      'name'      => $form->title(),
      'mail_tags' => $mail_tags,
    ];

    $results[] = $form_data;
  }

  return rest_ensure_response( $results );
}

function get_contact_form_by_id($request) {
  $form_id   = $request['id'];
  $form      = wpcf7_contact_form($form_id);
  $form_data = [
    'id'        => $form->id(),
    'name'      => $form->title(),
    'mail_tags' => []
  ];
  foreach ($form->scan_form_tags() as $tag) {
    if ($tag instanceof WPCF7_FormTag) {
      $form_data['mail_tags'][] = $tag;
    }
  }
  return $form_data;
}

function get_my_taxonomy_terms($taxonomy) {

  $return = [];

  $terms = get_terms($taxonomy, [
    'post_type'  => [ 'projeto' ],
    'fields'     => 'all',
    'hide_empty' => false,
  ]);

  foreach ($terms as $term) {
    $count_query = new WP_Query([
      'post_type' => 'projeto',
      'tax_query' => [
        [
          'taxonomy' => $taxonomy,
          'field'    => 'term_id',
          'terms'    => $term->term_id,
        ],
      ],
      'post_status' => 'publish',
      'fields' => 'ids'  // Recupera apenas os IDs para melhorar a performance
    ]);

    // Adicionar a contagem ao objeto do termo
    $term->count = $count_query->found_posts;
    $return[] = $term;
  }
  
  return $return;
}

function is_front_page_in_any_language($post_id) {
  $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );

  if(!empty($languages)) {
    foreach($languages as $l){
      $front_page_id = apply_filters( 'wpml_object_id', get_option('page_on_front'), 'page', false, $l['language_code']);
      if($front_page_id == $post_id) {
        return true;
      }
    }
  }
  return false;
}

function get_my_permalink($post_id, $post_id_in_lang,$lang) {
  $default_language = apply_filters('wpml_default_language', NULL );
  $is_front_page = is_front_page_in_any_language($post_id);

  if ($is_front_page) {
    $base_url = get_site_url() . '/';
    return $lang == $default_language ? $base_url :  $base_url . $lang;
  } else {
    return get_permalink($post_id_in_lang);
  }
  
}

function authenticate_user_by_username($username) {
  $user = get_user_by('login', $username);
  
  if (!$user) {
    return false;
  }

  wp_set_auth_cookie($user->ID);
  wp_set_current_user($user->ID);
  
  return true;
}

function maximum_api_filter($query_params) {
  $query_params['per_page']['maximum'] = 200;
  return $query_params;
}
add_filter('rest_projeto_collection_params', 'maximum_api_filter');

function filter_add_rest_orderby_params( $params ) {
	$params['orderby']['enum'][] = 'menu_order';
	return $params;
}
add_filter( 'rest_projeto_collection_params', 'filter_add_rest_orderby_params', 10, 1 );