module.exports = {
  content: ['./*.php', './**/*.php', './**/**/*.php', './resources/css/*.css'],
  theme: {
    backgroundSize: {
      'auto': 'auto',
      'cover': 'cover',
      'contain': 'contain',
      '100%': '100%',
      '110%': '110%'
    },
    transitionProperty: {
      'background-size': 'background-size',
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
      '4k': '1920px'
    },
    extend: {
      colors: {
        'gray-fir': '#808080',
        'gray-sec': '#BDBDBD',
        'gray-thi': '#DDDDDD',
        'gray-fou': '#4F4F4F',
        'gray-fiv': '#EEEEEE',
        'search-bg': '#212121',
      },
    },
    fontFamily: {
      sans: [
        "Akkurat LL, sans-serif",
        { fontFeatureSettings: '"salt", "ss03"' },
      ],
    },
    fontSize: {
      xs: '0.75rem',
      sm: '0.8rem',
      base: '1rem',
      lg: '1.125rem',
      'xl': ['1.25rem', {
        lineHeight: '130%',
        letterSpacing: '-0.025em',
      }],
      '2xl': ['1.5rem', {
        lineHeight: '2rem',
      }],
      '3xl': ['1.875rem', {
        lineHeight: '2.25rem',
      }],
      '4xl': ['2.25rem', {
        lineHeight: '2.5rem',
      }], 
      '5xl': ['3.125rem', {
        lineHeight: '105%',
        letterSpacing: '-0.05em',
      }],
      '6xl': ['3.75rem', {
        lineHeight: '105%',
        letterSpacing: '-0.05em',
      }],
      '7xl': ['4.5rem', {
        lineHeight: '105%',
        letterSpacing: '-0.05em',
      }],
      '8xl': ['6.25rem', {
        lineHeight: '105%',
        letterSpacing: '-0.05em',
      }],
    },
    letterSpacing: {
      tightest: '-.045em',
    }
  },
  plugins: [],
}