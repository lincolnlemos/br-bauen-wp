<?php
  // Custom post type
  add_action( 'init', 'registerCPTprojeto' );
  function registerCPTprojeto() {
    register_post_type( 'projeto', [
      'labels' => [
        'name'                => _x( 'Projetos', 'bsdev' ),
        'singular_name'       => _x( 'Projeto', 'bsdev' ),
        'add_new'             => _x( 'Adicionar novo', 'bsdev' ),
        'add_new_item'        => _x( 'Adicionar novo projeto', 'bsdev' ),
        'edit_item'           => _x( 'Editar projeto', 'bsdev' ),
        'new_item'            => _x( 'Novo projeto', 'bsdev' ),
        'view_item'           => _x( 'Ver projeto', 'bsdev' ),
        'search_items'        => _x( 'Buscar projeto', 'bsdev' ),
        'not_found'           => _x( 'Nenhum projeto encontrado', 'bsdev' ),
        'not_found_in_trash'  => _x( 'Nenhum projeto encontrado no Lixo', 'bsdev' ),
        'parent_item_colon'   => _x( 'Parent projeto:', 'bsdev' ),
        'menu_name'           => _x( 'Projetos', 'bsdev' ),
      ],
      'hierarchical'        => false,
      'taxonomies'          => [ 'categoria-projeto', 'area-projeto' ],
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'menu_position'       => 5,
      'menu_icon'           => 'dashicons-portfolio',
      'show_in_nav_menus'   => true,
      'publicly_queryable'  => true,
      'exclude_from_search' => false,
      'has_archive'         => true,
      'query_var'           => true,
      'can_export'          => true,
      'rewrite'             => [
        'slug' => 'work'
      ],
      'show_in_rest'        => true,
      'rest_base'          => 'projetos',
      'capability_type'     => 'post',
      'supports'            => [
        'title',
        'editor',
        'thumbnail',
        'revisions'
      ]
    ] );

    // register custom taxonomy
    register_taxonomy('categoria-projeto',[ 'projeto' ], [
      'hierarchical' => true,
      'labels' => [
        'name'              => _x( 'Categorias projetos', 'bsdev' ),
        'singular_name'     => _x( 'Categoria', 'bsdev' ),
        'search_items'      => __( 'Buscar categorias' ),
        'all_items'         => __( 'Todas categorias' ),
        'parent_item'       => __( 'Parent categoria' ),
        'parent_item_colon' => __( 'Parent categoria:' ),
        'edit_item'         => __( 'Editar categoria' ),
        'update_item'       => __( 'Atualizar categoria' ),
        'add_new_item'      => __( 'Adicionar nova categoria' ),
        'new_item_name'     => __( 'Nova categoria' ),
      ],
      'show_ui'             => true,
      'query_var'           => true,
      'show_in_rest'        => true,
    ]);

    register_taxonomy('area-projeto',[ 'projeto' ], [
      'hierarchical' => true,
      'labels' => [
        'name'              => _x( 'Áreas projetos', 'bsdev' ),
        'singular_name'     => _x( 'área', 'bsdev' ),
        'search_items'      => __( 'Buscar áreas' ),
        'all_items'         => __( 'Todas áreas' ),
        'parent_item'       => __( 'Parent área' ),
        'parent_item_colon' => __( 'Parent área:' ),
        'edit_item'         => __( 'Editar área' ),
        'update_item'       => __( 'Atualizar área' ),
        'add_new_item'      => __( 'Adicionar nova área' ),
        'new_item_name'     => __( 'Nova área' ),
      ],
      'show_ui'             => true,
      'query_var'           => true,
      'show_in_rest'        => true,
    ]);
  }