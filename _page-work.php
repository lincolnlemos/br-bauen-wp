<?php
	/**
	* Template Name: Work
	*
	* @package WordPress
	* @subpackage Twenty_Fourteen
	* @since Twenty Fourteen 1.0
*/

  get_header();
  _partials('_start');
?>
  
  <div class="flex flex-wrap items-start my-16 2xl:my-36 p-2.5">
    <div class="w-full md:w-6/12 mb-10 md:mb-0"  data-aos="fade-up">
      <h2 class="text-4xl xl:text-6xl 2xl:text-8xl"><?php the_title(); ?></h2>
    </div>

    <div class="w-full md:w-6/12 flex items-start justify-between" data-aos="fade-up" data-aos-delay="300">
      <?php _partials('_get-terms'); ?> 

      <div class="toggle-btns flex items-center">
        <svg onclick="toggleView(event);" class="cursor-pointer layout-large active" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="8" height="8" fill="#DDDDDD"/>
          <rect y="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" width="8" height="8" fill="#DDDDDD"/>
          <rect width="20" height="20" fill="#DDDDDD"/>
        </svg>
        
        <svg onclick="toggleView(event);" class="mx-2.5 cursor-pointer layout-small" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="8" height="8" fill="#DDDDDD"/>
          <rect y="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" width="8" height="8" fill="#DDDDDD"/>
          <rect x="12" y="12" width="8" height="8" fill="#DDDDDD"/>
        </svg>

        <svg onclick="toggleView(event);" class="cursor-pointer layout-list" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="20" height="2" fill="#DDDDDD"/>
          <rect y="6" width="20" height="2" fill="#DDDDDD"/>
          <rect y="12" width="20" height="2" fill="#DDDDDD"/>
          <rect y="18" width="20" height="2" fill="#DDDDDD"/>
        </svg>
      </div>
      
    </div>
  </div>
  
  <div class="flex flex-wrap items-between mt-36 2xl:mt-72 list__view">
    <?php
      $loop = new WP_Query( [
        'post_type' => 'projeto',
        'posts_per_page' => -1,
        'post_status' => [ 'publish', 'coming' ],
      ] );
      if ( $loop->have_posts() ) :
      $delay = 0; while ( $loop->have_posts() ) : $loop->the_post();
      $url = wp_get_attachment_url( get_post_thumbnail_id( $loop->ID ) );
      $current_status = get_post_status ( $loop->ID );

      // variables by post status
      if ( $current_status == 'coming' ) :
        $bgClass = 'bg-inherit';
        $link = '#';
        $cursor = 'cursor-auto';
        $btnTitle = $wpml_lang == 'en' ? 'Coming Soon' : 'Em Breve';
        $magicMouseHover = $wpml_lang == 'en' ? 'View Case' : 'Ver Projeto';

      else : 
        $bgClass = 'bg-black';
        $link = get_permalink( $loop->ID );
        $cursor = 'cursor-pointer';
        $btnTitle = $wpml_lang == 'en' ? '→ View Case Study' : 'Ver Projeto';
        $magicMouseHover = $wpml_lang == 'en' ? 'View Case' : 'Ver Projeto';

      endif;
    ?>
      <div class="p-1 w-6/12 ease-linear duration-300 view__grid" data-aos="fade-up" data-aos-offset="200" data-aos-delay="<?php echo $delay; ?>">
        <div class="overflow-hidden">
          <a 
            class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-100	transition-all duration-300 <?php echo $cursor; ?>"
            data-magicmouse-text="<?php echo $magicMouseHover; ?>"
            data-magicmouse-hover-class="size-120 bg-white"
            href="<?php echo $link; ?>"
            style="background-image: url(<?php echo $url; ?>);">
            <div class="<?php echo $bgClass; ?> w-full py-2 px-2.5 opacity-0 hover:opacity-100 transition-all duration-300">
              <?php
                // check post stats
                if ( $current_status != 'coming' ) :
                  $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'categoria-projeto' );
                  foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
                  echo '<p class="text-white">' .get_the_title( $loop->ID ). '</p>';
                  echo '<p class="text-gray-fir">' .implode(", ", $terms_name). '</p>';
                
                else :
                  echo '<p class="flex items-end h-full text-black">',
                    '<svg class="mr-2.5" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect width="8" height="8" fill="#000000"/>
                      <rect y="12" width="8" height="8" fill="#000000"/>
                      <rect x="12" width="8" height="8" fill="#000000"/>
                      <rect width="20" height="20" fill="#000000"/>
                    </svg>',
                  'In Progress</p>';
                
                endif;
              ?>
            </div>
          </a>
        </div>
      </div>

      <a class="view__list hidden w-full flex flex-wrap border-t border-gray-thi border-solid last:border-b text-xl text-black p-2.5 <?php echo $cursor; ?>" href="<?php echo $link; ?>">
        <div class="w-full md:w-3/12 font-black"><?php the_title() ?></div>
        <div class="w-full md:w-3/12">
          <?php
            $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'categoria-projeto' );
            foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
            echo implode(", ", $terms_name);
          ?>
        </div>
        <div class="w-full md:w-3/12">
          <?php
            $terms_name = []; $terms = wp_get_object_terms( get_the_ID(), 'area-projeto' );
            foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
            echo implode(", ", $terms_name);
          ?>
        </div>
        <div class="w-full md:w-3/12 text-left md:text-right hover-line"><?php echo $btnTitle; ?></div>
      </a>
    <?php $delay += 100; endwhile; endif; ?>
  </div>

  <?php
  _partials('_lets-work-together');
  _partials('_js-accordion-works');
  _partials('_end');
  get_footer();