<?php
  /* Template Name: Contact */
  get_header();
  _partials('_start');
  if ( have_posts() ) while ( have_posts() ) : the_post();
?>
  <article <?php post_class( 'page flex flex-wrap items-start pt-32 pb-44 px-2.5' ); ?>>
    <div class="flex flex-wrap w-full md:w-1/2" data-aos="fade-up">
      <div class="w-full mb-24">
        <?php
          $title = ( get_field('custom_title') ) ? get_field('custom_title') : get_the_title();
          echo '<h2 class="text-4xl xl:text-6xl 2xl:text-8xl text-black">'. $title .'</h2>';
        ?>
      </div>

      <div class="w-full md:w-1/2 flex flex-col">
        <?php the_content(); ?>
      </div>
      <div class="w-full md:w-1/2 mt-10 md:mt-0">
        <?php 
          $address = get_field('rpt_address', 'option' );
          if ( $address ) : 
        ?>
          <ul class="address">
            <?php foreach ($address as $addres) { ?>
              <li class="last:pt-8">
                <p class="font-black"><?php the_sub_field('state'); ?></p>
                <p><?php echo $addres['address']; ?></p>
                <p><?php echo $addres['phone']; ?></p>
              </li>
            <?php }; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>

    <div class="w-full md:w-1/2 mt-10 md:mt-0" data-aos="fade-up" data-aos-delay="300">
      <?php echo do_shortcode(get_field('shortcode_contact_form')); ?>
    </div>
  </article>
    
<?php
  endwhile;
  _partials('_end');
  get_footer();