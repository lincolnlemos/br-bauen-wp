<?php
  get_header();
  _partials('_start');
?>
  
  <?php 
    $projects = get_field('featured_projects');
    if ($projects) :
  ?>
    <div class="mt-16 md:mt-20 lg:mt-36 lg:mt-72">
      <h2 class="text-4xl xl:text-6xl 2xl:text-8xl mb-10" data-aos="fade-up" data-aos-offset="200" data-aos="fade-up" data-aos-delay="300"><?php the_field('projects_section_title'); ?></h2>
      <div class="flex flex-wrap items-between">
        <?php        
          $delay = 0; foreach ( array_slice($projects, 0, 4) as $project ) :
          $url = wp_get_attachment_url( get_post_thumbnail_id( $project->ID ) );
          $current_status = get_post_status ( $project->ID );          

          // variables by post status and language
          if ( $current_status == 'coming' ) :
            $bgClass = 'bg-inherit';
            $link = '#';
            $cursor = 'cursor-auto';

            if ( $wpml_lang == 'en' ) :
              $btnTitle = 'Coming Soon';
            else:
              $btnTitle = 'Em Breve';
            endif;

            // data text mouse hover
            $dataTxt = $btnTitle;

          else : 
            $bgClass = 'bg-black';
            $link = get_permalink( $project->ID );
            $cursor = 'cursor-pointer';
            $btnTitle = '→ View Case Study';

            // data text mouse hover
            if ( $wpml_lang == 'en' ) :
              $dataTxt = 'View Project';
            else:
              $dataTxt = 'Ver Projeto';
            endif;

          endif;
        ?>
          <div class="p-1 w-full sm:w-6/12 ease-linear duration-300 view__grid" data-aos="fade-up" data-aos-offset="200" data-aos-delay="<?php echo $delay; ?>">
            <div class="overflow-hidden" data-magicmouse-hover-class="size-120 bg-white" data-magicmouse-text="<?php echo $dataTxt; ?>">
              <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-100	transition-all duration-300 <?php echo $cursor; ?>" href="<?php echo $link; ?>" style="background-image: url(<?php echo $url; ?>);">
                <div class="<?php echo $bgClass; ?> w-full py-2 px-2.5 opacity-0 hover:opacity-100 transition-all duration-300">
                  <?php
                    // check post stats
                    if ( $current_status != 'coming' ) :
                      $terms_name = []; $terms = wp_get_object_terms( $project->ID, 'categoria-projeto' );
                      foreach ($terms as $term) : array_push($terms_name, $term->name); endforeach;
                      echo '<p class="text-white">' .get_the_title( $project->ID ). '</p>';
                      echo '<p class="text-gray-fir">' .implode(", ", $terms_name). '</p>';

                    else :
                      echo '<p class="flex items-end h-full text-black">',
                        '<svg class="mr-2.5" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <rect width="8" height="8" fill="#000000"/>
                          <rect y="12" width="8" height="8" fill="#000000"/>
                          <rect x="12" width="8" height="8" fill="#000000"/>
                          <rect width="20" height="20" fill="#000000"/>
                        </svg>',
                        $btnTitle,
                      '</p>';
                    
                    endif;
                  ?>
                </div>
              </a>
            </div>
          </div>
        <?php $delay+= 100; endforeach; ?>
      </div>
    </div>
  <?php endif; // $projects ?>

  <div class="flex items-strech mt-10 md:mt-20 2xl:mt-44 mb-20 md:mb-36 2xl:mb-72">
    <div class="w-full md:w-1/2 ml-auto">
      <div class="w-full 2xl:w-1/2">
        <p class="text-xl mb-20 2xl:mb-40" data-aos="fade-up" data-aos-offset="200"><?php the_field('excerpt_about_description'); ?></p>
        <div data-aos="fade-up" data-aos-offset="200">
          <a class="text-3xl xl:text-4xl 2xl:text-5xl hover-line 2xl:hover-line-4x" href="<?php echo get_permalink(2); ?>"><?php the_field('excerpt_about_title'); ?></a>
        </div>
      </div>
    </div>
  </div>

  <!-- maps -->
  <?php
    $mapsFront = get_field('front_page_maps');
    if ( $mapsFront == 1 ) :
      $query = new WP_Query([ 'page_id' => '2' ]);
      if ( $query->have_posts() ) : while ( $query->have_posts() ) :
        $query->the_post();
        echo '<article>';
        foreach ( get_field('page_block') as $item) {
          if ( $item['acf_fc_layout'] == 'we_are_working' ) {
            global $item;
            _partials('_we_are_working');
          }
        }
        echo '</article>';
      endwhile; endif;
      wp_reset_postdata();
    endif;
  ?>

  <!-- news -->
  <?php
    $loop = new WP_Query([
      'post_type'       => 'post',
      'posts_per_page'  => 2
    ]);
    if ( $loop->have_posts() ) :
  ?>
    <div class="flex flex-wrap justify-between pt-24 pb-10">
      <div class="w-full md:w-1/2" data-aos="fade-up" data-aos-offset="200">
        <h2 class="text-4xl xl:text-6xl 2xl:text-8xl mb-10">Notes, Stories<br/> & Insight</h2>
      </div>
      <div class="w-full md:w-1/2 flex flex-wrap">
        <?php
          while ( $loop->have_posts() ) : $loop->the_post();
          $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
          $categories = get_the_category();
        ?>
          <div class="sm:w-full md:w-1/2 p-2.5 text-black text-xl mb-20" data-aos="fade-up" data-aos-offset="200" data-aos-delay="150" data-magicmouse-hover-class="size-120" data-magicmouse-text="+">
            <?php
              // if has thumbnail
              if ( $url ):
            ?>
              <div class="overflow-hidden">
                <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-110	transition-all duration-300" href="<?php echo get_permalink($project->ID); ?>" style="background-image: url(<?php echo $url; ?>);"></a>
              </div>
            <?php endif; ?>

            <a href="<?php the_permalink(); ?>">
              <h2 class="text-2xl md:text-3xl lg:text-4xl 4k:text-5xl my-2.5">
                <span class="hover-line 4k:hover-line-4x"><?php the_title(); ?></span>
              </h2>
              <p class="text-xl">
                <?php
                  echo get_the_time('d.m.Y');
                  if ( $categories ) : 
                    echo ' — ' . esc_html( $categories[0]->name );
                  endif;
                ?>
              </p>
            </a>
          </div>
        <?php endwhile; ?>

        <div class="w-full mt-24" data-aos="fade-up" data-aos-offset="200" data-aos-delay="300">
          <a class="text-2xl md:tex-3xl lg:text-4xl 4k:text-5xl hover-line 4k:hover-line-4x" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">→ More Stories</a>
        </div>

      </div>
    </div>
  <?php endif; ?>

<?php
  _partials('_end');
  get_footer();