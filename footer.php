<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?>

	<footer id="footer" role="contentinfo" class="flex flex-col bg-black text-white p-2.5 text-base xl:text-xl font-normal" data-magicmouse-hover-class="bg-white">
		<div class="flex flex-wrap justify-between">
			<div class="w-full md:w-6/12 xl:w-3/12 2xl:w-1/4 flex flex-col items-start">
				<p class="font-black">New Business</p>
				<a class="hover-line hover-line__white" href="mailto:<?php $email = get_field('email_info', 'option'); echo $email; ?>?subject=<?php echo __('Fale Conosco', 'brbauen'); ?>" title="<?php echo __('Fale Conosco', 'brbauen'); ?>"><?php echo $email; ?></a>
				<a class="phone_with_ddd hover-line hover-line__white" title="Ligue para nós!" href="tel:<?php $phone = get_field('phone_info', 'option'); echo $phone; ?>"><?php echo $phone; ?></a>
			</div>

			<div class="w-full md:w-6/12 xl:w-4/12 2xl:w-1/4 mt-5 2xl:mt-0">
				<?php if ( have_rows( 'rpt_address', 'option' ) ) : ?>
					<ul class="address">
						<?php  while ( have_rows( 'rpt_address', 'option' ) ) : the_row(); ?>
							<li class="last:pt-8">
								<p class="font-black"><?php the_sub_field('state'); ?></p>
								<p><?php the_sub_field('address', 'options'); ?></p>
								<p><?php the_sub_field('phone', 'options'); ?></p>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>

			<div class="w-full md:w-6/12 xl:w-3/12 2xl:w-1/4 mt-5 2xl:mt-0">
				<nav>
					<?php
						wp_nav_menu([
							'menu'            => 'principal',
							'container'       => 'ul',
							'theme_location'  => 'top',
							'menu_class'     	=> 'menu menu__header flex flex-col',
							'add_li_class'		=> ''
						]);
					?>
				</nav>
			</div>

			<div class="w-full md:w-6/12 xl:w-2/12 2xl:w-1/4 mt-5 2xl:mt-0">
				<ul class="flex flex-col items-start socials">
					<li><a class="font-black hover-line hover-line__white" target="_blank" href="<?php the_field('facebook', 'options'); ?>">Facebook</a></li>
					<li><a class="font-black hover-line hover-line__white" target="_blank" href="<?php the_field('instagram', 'options'); ?>">Instagram</a></li>
					<li><a class="font-black hover-line hover-line__white" target="_blank" href="<?php the_field('behance', 'options'); ?>">Behance</a></li>
				</ul>
			</div>
		</div>

		<div class="flex flex-wrap justify-between mt-16 md:mt-32">
			<div class="w-full md:w-2/12 xl:w-3/12 2xl:w-1/4 mt-5 2xl:mt-0">
				<p>© <?php bloginfo( 'name' ); ?></p>
			</div>
			<div class="w-full md:w-4/12 xl:w-6/12 2xl:w-1/2 mt-5 2xl:mt-0">
				<a href="#" id="js-show-news" class="hover-line">↳ Subscribe to our newsletter</a>
			</div>
			<div class="w-full md:w-6/12 xl:w-3/12 2xl:w-1/4 mt-5 2xl:mt-0">
				<a class="hover-line" href="<?php echo get_permalink(); ?>">Privacy Policy</a>
			</div>
		</div>
	</footer>

	<div id="js-news-content" class="relative flex items-center justify-center bg-search-bg overflow-hidden w-full z-50">
		<div class="w-1/2">
			<?php echo do_shortcode('[contact-form-7 id="1557" title="Newsletter"]'); ?>
		</div>
	</div>

	<?php wp_footer(); ?>
	<!-- <script>
		jQuery(document).ready(function ($) {
			$('.loader').delay(1000);
			setTimeout(() => {
				$('.loader').hide();
			}, 900);
		});
	</script> -->
</body>
</html>