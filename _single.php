<?php
  get_header();
  _partials('_start');
  if ( have_posts() ) while ( have_posts() ) : the_post();
  $author_id = get_post_field( 'post_author');
?>  

  <div class="flex flex-wrap mt-10 md:mt-24">
    <div class="w-full flex flex-wrap content-scroll">
      <div id="sidebar" class="w-full md:w-1/2">
        <div class="sidebar__inner">
          <?php the_title('<h2 class="text-4xl xl:text-6xl 2xl:text-8xl">','</h2>'); ?>
          <div class="my-10 md:my-24 text-xl">
            <div class="flex"><span class="w-1/6">Written By</span> <?php echo get_the_author_meta('display_name', $author_id);; ?></div>
            <div class="flex mt-10">
              <span class="w-1/6">Share</span>
              <?php echo do_shortcode('[social-share]'); ?>
            </div>
          </div>
        </div>
      </div>
      
      <article <?php post_class( 'w-full md:w-1/2 max-w-4xl single' ); ?>>
        <div class="content">
          <?php the_content(); ?>
        </div>
      </article>
    </div>
    
    <?php
      $loop = new WP_Query([
        'post_type'       => 'post',
        'post__not_in'    => [get_the_ID()],
        'posts_per_page'  => 2
      ]);
      if ( $loop->have_posts() ) :
    ?>
      <div class="w-full mt-48 pt-10 border-t-2 border-black"></div>
      <div class="w-full md:w-1/2 text-6xl xl:text-6xl 2xl:text-6xl 4k:text-8xl">Up Next</div>
      <div class="w-full md:w-1/2 flex flex-wrap">
        <?php
          while ( $loop->have_posts() ) : $loop->the_post();
          $categories = get_the_category();
        ?>
          <div class="sm:w-full md:w-1/2 p-2.5 text-black text-xl mb-10 md:mb-20">
            <a href="<?php the_permalink(); ?>"><figure class="overflow-hidden"><?php the_post_thumbnail( 'full', ['class' => 'w-full ease-linear duration-300 hover:scale-110'] ); ?></figure></a>
            <a href="<?php the_permalink(); ?>">
              <h2 class="text-3xl md:text-4xl xl:text-5xl 2xl:text-5xl 4k:text-5xl my-2.5 hover-line 2xl:hover-line-4x"><?php the_title(); ?></h2>
              <p class="text-xl">
                <?php
                  echo get_the_time('d.m.Y');
                  if ( $categories ) : 
                    echo ' — ' . esc_html( $categories[0]->name );
                  endif;
                ?>
              </p>
            </a>
          </div>
        <?php endwhile; ?>

        <div class="flex justify-start">
          <a class="w-full text-3xl md:text-4xl lg:text-5xl my-16 md:my-36 hover-line lg:hover-line-4x" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">↖ Back to news</a>
        </div>
      </div>
    <?php endif; ?>
  </div>

  <script type="text/javascript">
    var sidebar = new StickySidebar('#sidebar', {
      topSpacing: 20,
      bottomSpacing: 20,
      containerSelector: '.content-scroll',
      innerWrapperSelector: '.sidebar__inner'
    });
  </script>

<?php
  endwhile;
  _partials('_end');
  get_footer();