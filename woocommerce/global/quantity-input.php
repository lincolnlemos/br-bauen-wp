<?php
/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.2.0
 */

defined( 'ABSPATH' ) || exit;

/* translators: %s: Quantity. */
$label = ! empty( $args['product_name'] ) ? sprintf( esc_html__( '%s quantity', 'woocommerce' ), wp_strip_all_tags( $args['product_name'] ) ) : esc_html__( 'Quantity', 'woocommerce' );

// In some cases we wish to display the quantity but not allow for it to be changed.
if ( $max_value && $min_value === $max_value ) {
	$is_readonly = true;
	$input_value = $min_value;
} else {
	$is_readonly = false;
}
?>
  <div class="quantity flex flex-col justify-end">
    <?php
      /**
       * Hook to output something before the quantity input field.
       *
       * @since 7.2.0
       */
      do_action( 'woocommerce_before_quantity_input_field' );
    ?>
    <label for="<?php echo esc_attr( $input_id ); ?>" class="font-black w-full block mb-1 pb-px"><?php echo __('Quantity', 'brbauen'); ?></label>
    <div class="flex flex-row md:w-full ml-auto md:ml-0">
      <input
        type="<?php echo $is_readonly ? 'text' : 'number'; ?>"
        <?php wp_readonly( $is_readonly ); ?>
        id="<?php echo esc_attr( $input_id ); ?>"
        class="text-xl mr-2 <?php echo esc_attr( join( ' ', (array) $classes ) ); ?>"
        name="<?php echo esc_attr( $input_name ); ?>"
        value="<?php echo esc_attr( $input_value ); ?>"
        title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
        size="4"
        min="<?php echo esc_attr( $min_value ); ?>"
        max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
        <?php if ( ! $is_readonly ): ?>
          step="<?php echo esc_attr( $step ); ?>"
          placeholder="<?php echo esc_attr( $placeholder ); ?>"
          inputmode="<?php echo esc_attr( $inputmode ); ?>"
          autocomplete="<?php echo esc_attr( isset( $autocomplete ) ? $autocomplete : 'on' ); ?>"
        <?php endif; ?>
      />
      <div class="arrows flex flex-col justify-center space-y-2">
        <button type="button" class="btn btn-light btn-plus">
          <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 11H12L5.99429 0L0 11Z" fill="black"/></svg>
        </button>
        <button type="button" class="btn btn-light btn-minus">
          <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 0L-1.91532e-06 1.04907e-06L6.00571 11L12 0Z" fill="black"/></svg>
        </button>
      </div>
    </div>
    <?php
      /**
       * Hook to output something after quantity input field
       *
       * @since 3.6.0
       */
      do_action( 'woocommerce_after_quantity_input_field' );
    ?>
  </div>
<?php