<?php
  _partials('_header-notes');
?>
  <article <?php post_class( 'flex flex-wrap' ); ?>>
    <?php
      if ( have_posts() ) while ( have_posts() ) : the_post();
      $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
    ?>
      <div class="w-3/12 p-2.5 text-black text-xl mb-20">
        <div class="overflow-hidden">
          <a class="flex w-full text-white aspect-square bg-no-repeat bg-center bg-cover bg-cover scale-100 hover:scale-110	transition-all duration-300" href="<?php echo get_permalink($project->ID); ?>" style="background-image: url(<?php echo $url; ?>);"></a>
        </div>
        <a href="<?php the_permalink(); ?>">
          <h2 class="text-5xl my-2.5 hover:underline underline-offset-8 "><?php the_title(); ?></h2>
          <p class="text-xl">
            <?php
              echo get_the_time('d.m.Y');
              if ( $categories ) : 
                echo ' — ' . esc_html( $categories[0]->name );
              endif;
            ?>
          </p>
        </a>
      </div>
    <?php endwhile; ?>
  </article>
  
<?php
  $posts_page_id = get_option( 'page_for_posts' );
  echo '<a class="text-4xl xl:text-6xl 2xl:text-8xl" href="'.get_permalink($posts_page_id).'">'. __('Back to all notes', 'brbauen') .'<br />↖</a>';
  _partials('_end');
  get_footer();